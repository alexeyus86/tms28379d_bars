//#include "DSP281x_Device.h"				// ������������� �����������
#include "Defines_Common.h"				// ����� ����������
#include "Defines_CAN.h"

//#ifdef FRM_ALGO_v2

#include "Defines_FRM.h"				// ��������� ������ ��� ���
#include "fir.h"						// ������

//#if (VER_FRM == 2338)

int use_filter = 0;
float filterCalc(float input, float b[], float a[]);
//float a_coef[N_FTR] = { -0.00016209,-2.4506e-06,7.8793e-05,0.0001589,0.00017276,8.8603e-05,-6.0065e-05,-0.0001866,-0.00020425,-8.8848e-05,9.6155e-05,0.00023468,0.00023664,0.00010024,-8.342e-05,-0.00019516,-0.00017482,-6.2158e-05,3.9328e-05,5.327e-05,-1.3468e-06,-2.5081e-05,6.5387e-05,0.00023738,0.00033219,0.00017962,-0.00023517,-0.00068965,-0.00082496,-0.00040019,0.00046988,0.0012912,0.0014577,0.00067396,-0.0007508,-0.0019874,-0.0021647,-0.00096884,0.0010423,0.0026807,0.0028379,0.001236,-0.0012918,-0.0032354,-0.0033341,-0.0014133,0.0014353,0.0034935,0.0034934,0.0014345,-0.0014073,-0.0033003,-0.0031665,-0.0012409,0.0011534,0.0025365,0.0022486,0.00079585,-0.00064465,-0.0011527,-0.00071292,-9.8501e-05,-0.00010932,-0.00080004 };
//float b_coef[N_FTR] = { 0.056372,0.023273,-0.023121,-0.055265,-0.054534,-0.022213,0.021773,0.051328,0.049941,0.02005,-0.019367,-0.044961,-0.043056,-0.017002,0.016143,0.036797,0.034562,0.013368,-0.012415,-0.027623,-0.025268,-0.0094901,0.0085303,0.018281,0.016011,0.0057118,-0.0048248,-0.0095599,-0.007556,-0.0023361,0.0015869,0.0021155,0.00051114,-0.00040558,0.00097342,0.0036016,0.00473,0.0023748,-0.002742,-0.0073765,-0.008013,-0.0035325,0.0037039,0.0092304,0.0094143,0.0039323,-0.003933,-0.0093889,-0.0092028,-0.0037019,0.0035716,0.0082266,0.0077799,0.0030168,-0.0028027,-0.0061988,-0.0056099,-0.0020714,0.001821,0.0037717,0.0031515,0.0010508,-0.00080587,-0.001361 };


#define DEBUG   0

int alg_sens = 0;

//typedef _Bool bool;

#define FRM_ALGORITM    0x11

#pragma DATA_SECTION(ARSF_filter, "firfilt");
static FIR16 ARSF_filter;                                                               // ������ ��� ������
#pragma DATA_SECTION(ARSF_filter_dbuffer,"Filter_Buf");
int32 ARSF_filter_dbuffer[(ARSF_filter_v2_ORDER + 2) / 2];// ����� �������� ����������� ��� ������ ��� �������
int32 const ARSF_filter_coeff[(ARSF_filter_v2_ORDER + 2) / 2] = ARSF_filter_v2_FIR16_COEFF; // ������������ ��� �������

#define ARSF_LevelZero  0                   // ��������������� �����
#define ARSF_LevelHi    ARSF_LevelZero+5    // ������� ������� ������������� ����
#define ARSF_LevelLow   ARSF_LevelZero-5    // ������ ������� ������������� ����
#define ARSF_LevelDetect_Low    20          // ������� ����������� ����
#define ARSF_LevelDetect_Hi     100         // ������� ����������� ����
//#define ARSF_Size_Ampl_Buf        128
#define ARSF_Level_ARU_Step_Up      800     // ������� ������� ��� ��������� ����.���
#define ARSF_Level_ARU_Step_Down    2000    // ������� ������� ��� ��������� ����.���

// ����� ������ ������� - ������ ��������� �������� ����
#define ARSF_TIME_NoFRM_Update  26786L      // ����� ���������� ��� �� ~400��
#define ARSF_TIME_NoRecieveKod  50223L      // ���������� ����� ���������� ���� ~750��
#define ARSF_TIME_AfterKod      6696L       // ����� "������� ����" ����� �������� ���� ~100��
#define ARSF_NOISE_AMPL_DETECT      1       // ���� �� ���������������� ��� ����������� ������ ����

static Uint32 ARSF_CycleWork;       // ������� ����������� ���
static Uint32 ARSF_Time_Limit;

#pragma DATA_SECTION(ARSF_KodDemod, "ARSF_KodDemod");
unsigned char ARSF_KodDemod[6 * 8 + 1], ARSF_indexDemod;

static unsigned char ARSF_FazeKodAbs;   // ���������� ����� ���� �� ������
static char ARSF_FreqCorrect;           // ���������� �������
static unsigned char ARSF_LastLevel;    // ���������� � ���������� ������
static unsigned char ARSF_FazeDetect;   // ������� ����������� ���� � �������

static int16 ARSF_AmplMax, ARSF_AmplMin;    // ������������ ���������

Uint16 ARSF_Amplituda;                                      // ��������� ������� �� ������ ������� �� ������ 12��
static Uint32 ARSF_AmplitudaAvgSum;
static Uint16 ARSF_AmplitudaAvgIndex;
/*static Uint32 ARSF_AmplitudaSum;                          // ����� �������� ������� �� ������ ������� �� ������ 12��
static Uint16 ARSF_AmplitudaMassive[ARSF_Size_Ampl_Buf];    // ������ �������� ������� �� ������ �������
static Uint16 ARSF_AmplitudaIndex;                          // ������ ������� �������� ������� �� ������ ������� */

int16 ARSF_LevelNoise;                              // ��������������� ��������� ����
int16 ARSF_LevelDetect;                             // ������� ������������� ��������� �������
/*static*/ int16 ARSF_EndFRM;                           // ������� ������ ��������������� �������

/**
 * @brief ����������� ��������� ������������� ����
 * @category Frm FRM_defines.h "Include\FRM_defines.h"
 */

//extern unsigned int Light;                    // ������������ ���������

#include "stdbool.h"

int16 calc_tmp_ampl = 0;
float max_tmp_ampl = 0;
float max_tmp_rms = 0;
float tmp_ampl = 0;
float tmp_rms = 0;

////////////////
int32 count_frm_code = 0;
int16 is_code = 0;
////////////////

#define N_FFT           128
#define FFT_CALC        0

#if FFT_CALC
#define NEW_FFT         1
#define SNR_CALC        1
#endif

#define DEBUG_FRM       1

#define TH_FRM_ARU		0
#define LIGHT			0
#define FILTERING       1
#define N_RMS   		128
#define VER_RMS_BUF     1
#define SENS			0
#define FALSE_OPERATION	0
#define ALIKE   		1
#define SHIFT           1
#define USE_FLOAT_RMS   1
#define THH_METH        1
#define FC_ALL_TIME 	0
#define AMP_FC          0

#define ARU_CONST       0
#if ARU_CONST
#define ARU_INV         0
#define ARU_COEF        0
//#define ARU_COEF        0
#else
#define ARU_INV         0
#endif //ARU_CONST

#if ALIKE
int16 new_pack_find = 1;
#endif

///////////////////
//#define ARU_CONST       0
//#define ARU_COEF        2
//#define ARU_INV         1


//#if DEBUG_FRM
//#define SHOW_ALL_PACK	1
//#endif

volatile struct FRM ARSF_Struct;
volatile struct FRM ARSF_Struct_1;
volatile struct FRM ARSF_Struct_2;

char ARU_coef = 0;		// ����������� ���
char ARU_update = 0;	// ���������� ���

typedef char int8;
int16 clac_alg2 = 1;

int16 temmp = 4;

#if ALIKE
static int16 alike_v2(Uint8 *, Uint8 , int8 *);
#endif //ALIKE
static float rms_amplf(int16*, int16);

#if FALSE_OPERATION // ������� ������ ������ ������������
static Uint32 ARSF_CycleWork_frm_count  = 0;
static Uint32 ARSF_Time_Limit_frm_count = 1000000L;
unsigned long long frm_count = 0;
#endif

#include "math.h"
static int16 thh_calc = 0;
//static int16 thh = 1;
int16 thh_2 = 10;
//int16 thh_2 = 100;
static int16 th1 = 200;
static int16 th2 = 100;
static int16 rms_ampl = 0;

//static int16 buffer_bef_fil[N_RMS];
//static int16 is_buffer_bef_fil_full = 0;

static int16 buffer_aft_fil[N_RMS];
static int16 is_buffer_aft_fil_full = 0;

static int16 Point;

//extern Uint16 Number_FFK;
#define USE_LONG_FRM    1

#define N1_FFT	(N_FFT >> 1) // ����� ���
#define N2_FFT	(N_FFT >> 2) // ����� ����������

//static Uint16 temp_sh = 8;

#if FFT_CALC
#include "FFT.h"
#else
#include <stdbool.h>
typedef struct {int32 Re; int32 Im;} Complex;
#endif //FFT_CALC
#include <math.h>

/*static*/ float ampl = 0;
//#if !TH_FRM_ARU
//#if NEW_CALC_FRM
//static float th_ampl = 100;
//#else
//static float th_ampl = 1;
//#endif //NEW_CALC_FRM
//#endif //!TH_FRM_ARU

static float th_ampl = 0.1; // !!!

static Complex  Y[N1_FFT];
#if FFT_CALC
static int32    E[N1_FFT];
static Uint32 snr      = 0;
static Uint32 maxFc    = 0;
static int16  snr_ver  = 0;
#endif //FFT_CALC
static Uint16 Validity = 0;

static Uint32 ARSF_CycleWork_2  = 0;
static Uint32 ARSF_Time_Limit_2 = ARSF_TIME_NoRecieveKod; //N_FFT = 256

extern void* memset();

#ifdef _FLASH
#pragma CODE_SECTION(ARSF_Update,  ".TI.ramfunc");
#pragma CODE_SECTION(ARSF_Receive, ".TI.ramfunc");
#endif

#if USE_FLOAT_RMS
//static float rms_ampl_bef_fil = 0;
//static float rms_ampl_aft_fil = 0;
static float rms_amplf(int16 *, int16);
//static float rms_amplf2(buffer_N_RMS2);
#else
Uint16 rms_ampl_bef_fil = 0;
Uint16 rms_ampl_aft_fil = 0;
static Uint16 rms_amplf(int16 *, int16);
#endif

static void  Receive(int16);
static void  Init(void);
static int16 Poll(void);
static int16 FindPack(int16);
static int16 GetCmd(int16);
static int16 CmdDetect2(int16, Uint16);
#if SHIFT
static void mas_shift(Uint16 *);
#endif //SHIFT

#if FFT_CALC
int16 aruOn      = 0;
int16 filtering  = 1;
int16 cutting    = 1;
int16 averaging  = 0;

float temp_av    = 2;
#endif //FFT_CALC

//int16 TH_ARU_HI = 1500;
//int16 TH_ARU_LO = 500;
//
//int16 thh_calc  = 0;
//#if USE_FLOAT_RMS
//float thh       = 10;
//#else
//int16 thh       = 10;
//#endif

//char ARU_coef;
//char ARU_update;

Uint16 amp = 0;
int16  amp_buf[N_FFT];
int16  amp_buf2[N_FFT];
int16  amp_buf2_disp[N_FFT];

#if   N_FFT == 128
const int32 coef_1 = 0;
const int32 coef_2 = 1024;
#elif N_FFT == 256
const int32 coef_1 = 724;
const int32 coef_2 = 724;
#endif//N_FFT

static float calc_FRM(int16 *, int16);
static float calc_FRM2(int16 *, int16);

Uint16 ARSF_Amplituda;
int16  ARSF_LevelNoise;
int16  ARSF_LevelDetect;

//static Uint32 mean_rms_ampl = 0;
//static Uint32 max_rms_ampl  = 0;
//static Uint32 min_rms_ampl  = 0;

#if SENS
static int16 Cmd2 = 0;
static int16 Cmd3 = 0;
#endif //SENS

///////////////////////

#if   N_FFT == 128
#define T             (4)
#elif N_FFT == 256
#define T             (8)
#endif

#define Fc            (3348.21)

#define Fn1           (2700)
#define Fn2           (4000)
#define Nc            (int)(N_FFT / T)
#define Ni1           (int)(Fn1 / Fc * N_FFT / T)
#define Ni2           (int)(Fn2 / Fc * N_FFT / T)
#define N_F1          (int)(Fn1 / Fc * N_FFT / T)
#define N_F2       	  (int)(Fn2 / Fc * N_FFT / T)

#define BORD          (4 * T)
#define MIN_STEPS     (9)
#define MAX_TRY       (600)
#define MIN_SNR       (10)
#define K             (0.3)
//#define START_SNR     (int)((MIN_SNR << 7) / K )
#define START_SNR     (13000)

#define LEN_BURST     (T * 16)
#define STEP          (N_FFT - LEN_BURST)
#define LEN_CMD       (8 * LEN_BURST)
#if N_FFT < 256
#define LEN_PBUFF     (50 * STEP)
#else
#define LEN_PBUFF     (20 * STEP)
//#define LEN_PBUFF     (10 * STEP)
#endif
#define LEN_QUEUE     (20)
#define LEN_INBUFF    (LEN_QUEUE * STEP + STEP )
#define STEP_TRY      (2 * T)
#define Fs            (T * Fc)

#ifdef _FLASH
#pragma CODE_SECTION(Bauer,    ".TI.ramfunc");
#pragma CODE_SECTION(CmdDetect,".TI.ramfunc");
#pragma CODE_SECTION(CmdDetect2,".TI.ramfunc");
#pragma CODE_SECTION(GetCmd,   ".TI.ramfunc");
#pragma CODE_SECTION(FindPack, ".TI.ramfunc");
#pragma CODE_SECTION(Receive,  ".TI.ramfunc");
#pragma CODE_SECTION(Poll,     ".TI.ramfunc");
#endif

int isPack = 0;
int isMsg = 0;
int isOverlay = 0;

inline void IsPack(void)    {isPack = 1;}
inline void IsMsg(void)     {isMsg = 0;}
inline void IsOverlay(void) {isOverlay = 0;}

int16 InBuff  [LEN_INBUFF];
int16 PackBuff[LEN_PBUFF ];

volatile Uint16 HeadQueue = 0;
volatile Uint16 LenQueue = 0;

Uint16 StepCnt = 0;
Uint32 aSNR = START_SNR;

Uint16 TryCount = 0;

static Uint8 Bauer (Uint8 x)
{
  Uint8 b1 = x & 0xF;
  Uint8 b2 = x >> 4;
  switch (b2)
  {
  case 0:  if (b1==1 ) return b2; break;
  case 1:  if (b1==15) return b2; break;
  case 2:  if (b1==12) return b2; break;
  case 3:  if (b1==2 ) return b2; break;
  case 4:  if (b1==10) return b2; break;
  case 5:  if (b1==4 ) return b2; break;
  case 6:  if (b1==7 ) return b2; break;
  case 7:  if (b1==9 ) return b2; break;
  case 8:  if (b1==6 ) return b2; break;
  case 9:  if (b1==8 ) return b2; break;
  case 10: if (b1==11) return b2; break;
  case 11: if (b1==5 ) return b2; break;
  case 12: if (b1==13) return b2; break;
  case 13: if (b1==3 ) return b2; break;
  case 14: if (b1==0 ) return b2; break;
  case 15: if (b1==14) return b2; break;
  }
  return (0xFF);
}

static int16 CmdDetect(int16 aCmdHead, Uint16 delta)
{
    Uint16 i, j, k, Tr;
    Uint16 phTrans[8];
    Uint8  b1 = 0, b2 = 0;
    for (k = 0; k < 8; k++)
    {
        Uint16 o1 = aCmdHead + (k - 1) * LEN_BURST + delta;
        int32 Max = 0x80000000;
        for (i = 0; i < 4; i++)
        {
            Uint16 o2 = o1 + LEN_BURST + i * (T / 4);
            int32 Sum = 0;
            for (j = BORD; j < LEN_BURST - BORD - T; j++)
            {
                Sum += ((int32) PackBuff[o1 + j] * (int32) PackBuff[o2 + j]) >> 14;
            }
            if (Sum > Max)
            {
                phTrans[k] = i;
                Max = Sum;
            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        Tr = phTrans[i];
        if /**/ (Tr == 2) Tr = 3;
        else if (Tr == 3) Tr = 2;
        b1 = (b1 << 1) + (Tr >>  1);
        b2 = (b2 << 1) + (Tr & 0x1);
    }
    b1 = Bauer(b1);
    b2 = Bauer(b2);
#if SHIFT
    for (j = 0; j < 8; j++)
    {
        if ((b1 == 0xFF) || (b2 == 0xFF))
        {
            b1 = 0;
            b2 = 0;
            mas_shift(phTrans);
            for (i = 0; i < 8; i++)
            {
                Tr = phTrans[i];
                if /**/ (Tr == 2) Tr = 3;
                else if (Tr == 3) Tr = 2;
                b1 = (b1 << 1) + (Tr >>  1);
                b2 = (b2 << 1) + (Tr & 0x1);
            }
            b1 = Bauer(b1);
            b2 = Bauer(b2);
        }
        else break;
    }
#endif
    if (!((b1 == 0xFF) || (b2 == 0xFF))) return ((b1 << 4) + b2);
    else return (-1);
}

static int16 GetCmd(int16 aCmdHead)
{
    Uint16  i, j;
    Uint16  LimTry = MAX_TRY;
    int16   Msg = -1;
    int16   aMsg[2][5] = {{-1,-1,-1,-1,-1}, {0,0,0,0,0}};
    Uint16  delta = 0;
//    if (Number_FFK == 8) delta = T / 4;
    TryCount = 0;
    for (i = 0; i < 4; i++, aCmdHead -= LEN_CMD)
    {
        while ((aCmdHead >= LEN_BURST) && (TryCount < LimTry))
        {
            int16 Mes = CmdDetect2(aCmdHead, delta);
#if !LIGHT
            if (Mes < 0) Mes = CmdDetect2(aCmdHead, T / 4);
            if (Mes < 0) Mes = CmdDetect2(aCmdHead, T / 2);
#endif // !LIGHT
            if (Mes >-1)
            {
                if (i == 0) LimTry = TryCount + 24;
                for (j = 0; j < 5; j++)
                {
                    if (Mes == aMsg[0][j])
                    {
                        aMsg[1][j]++;
                        break;
                    }
                    else if (aMsg[0][j] == -1)
                    {
                        aMsg[0][j] = Mes;
                        aMsg[1][j] = 1;
                        break;
                    }
                }
                break; // from while
            }
            TryCount++;
            aCmdHead -= STEP_TRY;
        }
    }
    Validity = 0;
    for (i = 0; i < 4; i++)
    {
        if (aMsg[1][i] > Validity)
        {
            Validity = aMsg[1][i];
            Msg = aMsg[0][i];
        }
    }
    if (Msg >= 0) IsMsg();
    return (Msg);
}

static int16 FindPack(int16 aStart)
{
    static int16 isSig = false;
    //Uint32 snr = 0;
    int16  i = 0, j = 0, ii = 0;
    int32* X = (int32*)Y;
    int16* pBuf = PackBuff + StepCnt*STEP;
    int16  rCmdHead = StepCnt * STEP - LEN_CMD - LEN_BURST;
    if (StepCnt * STEP > (LEN_PBUFF - N_FFT)) return (rCmdHead);
    if (aStart < 0) aStart += LEN_INBUFF;
	memset(Y, 0, sizeof(Y));
	for (ii = 0; ii < N_FFT; ii++)
	{
		amp_buf[ii] = InBuff[aStart + ii];
	}
#if FFT_CALC

    while (i < STEP)
	{
		Y[Index[j]].Re = InBuff[aStart++];
//        amp_buf[j] = InBuff[aStart];
		i++;
		if (i < STEP) Y[Index[j++]].Im = InBuff[aStart++]/*, amp_buf[j] = InBuff[aStart]*/;
		i++;
		if (aStart >= LEN_INBUFF) aStart = 0;
	}
    DFFT(Y);
    if (cutting)
    {
        for (ii = 0; ii < N1_FFT; ii++)
        {
            if ((ii < N_F1) || (ii > N_F2))
            {
                Y[ii].Re = 0;
                Y[ii].Im = 0;
            }
        }
    }
    if (averaging)
    {
        for (ii = 0; ii < N1_FFT; ii++)
        {
            if (abs(Y[ii].Re + Y[ii].Im) > temp_av * maxFc)
            {
                Y[ii].Re = (Y[ii].Re / (4 * temp_av));
                Y[ii].Im = (Y[ii].Im / (4 * temp_av));
            }
        }
    }
    Power(E, Y);
    maxFc = abs(Y[Nc].Re + Y[Nc].Im);
#else
    while (i < STEP)
    {
        Y[j].Re = InBuff[aStart++];
//        amp_buf[j] = InBuff[aStart];
        i++;
        if (i < STEP) Y[j++].Im = InBuff[aStart++]/*, amp_buf[j] = InBuff[aStart]*/;
        i++;
        if (aStart >= LEN_INBUFF) aStart = 0;
    }
#endif //FFT_CALC

#if SNR_CALC
    if (snr_ver == 1)
    {
        for (i = Ni1; i < Ni2; i++) snr += E[i];
        snr = (Ni2 - Ni1 + 1) * (E[Nc - 1] + E[Nc] + E[Nc + 1]) / (snr - E[Nc - 1] - E[Nc] - E[Nc + 1] + 1);
    }
    else if (snr_ver == 2)
    {
        for (i = N_F1; i < N_F2; i++) snr += E[i];
        snr = (N_F2 - N_F1 + 1) * (E[Nc - 1] + E[Nc] + E[Nc + 1]) / (snr - E[Nc - 1] - E[Nc] - E[Nc + 1] + 1);
    }
    else if (snr_ver == 0)
    {
//        for (i = Ni1; i < N1_FFT; i++) snr += E[i] * 10/ thh;
        for (i = Ni1; i < N1_FFT; i++) snr += E[i];
        snr = (N1_FFT - Ni1 + 1) * (E[Nc - 1] + E[Nc] + E[Nc + 1]) / (snr - E[Nc - 1] - E[Nc] - E[Nc + 1] + 1);
    }
    aSNR = aSNR - (aSNR >> 7) + snr;
    if (aSNR < START_SNR) aSNR = START_SNR;

    if ((snr << 7) > K * aSNR)
    {
#else
    ampl = calc_FRM(amp_buf, N_FFT);

    static int16 count = 0;
    if (calc_tmp_ampl)
    {
        if (count++ < 10){
        tmp_ampl = calc_FRM2(amp_buf, N_FFT)/powf(2, ARU_coef);
        tmp_rms = rms_amplf(amp_buf, N_FFT)/powf(2, ARU_coef);

        if (tmp_ampl > max_tmp_ampl) max_tmp_ampl = tmp_ampl;
        if (tmp_rms > max_tmp_rms) max_tmp_rms = tmp_rms;
        }
        else
        {
            count = 0;
            max_tmp_ampl = 0;
            max_tmp_rms = 0;
        }
    }

#if TH_FRM_ARU
    if (ampl > ARU_coef * 10)
//    if (ampl > ARU_coef * 100)
#else
    if (ampl > th_ampl)
#endif //TH_FRM_ARU
    {
#endif //SNR_CALC

#if FFT_CALC
        IFFT(Y);
#endif
        for (i = 0; i < N_FFT; i++, X++, pBuf++) *pBuf += *X;
        StepCnt++;
        isSig = true;
//        Amplituda_Fc_8bit = maxFc >> temp_sh;
    }
    else if (isSig)
    {
#if FFT_CALC
        IFFT(Y);
#endif
        for (i = 0; i < N_FFT; i++, X++, pBuf++) *pBuf += *X;
        StepCnt++;
        isSig = false;
    }
    else if (StepCnt > MIN_STEPS)
    {
        IsPack();
        return (rCmdHead);
    }
    else
    {
        isPack = 0;
        if ((StepCnt * STEP + N_FFT) < LEN_PBUFF)
            memset(PackBuff, 0, (StepCnt * STEP + N_FFT) * (sizeof(int16)));
        else
            memset(PackBuff, 0, (LEN_PBUFF) * (sizeof(int16)));
        StepCnt = 0;
    }
    return (-1);
}

static void Receive(int16 Data)
{
    static Uint16 Head = 0;
    static Uint16 aCnt = 0;
    InBuff[Head++] = Data;
    if (++aCnt >= STEP)
    {
        aCnt = 0;
        HeadQueue = Head;
        if (LenQueue++ > LEN_QUEUE) IsOverlay();
    }
    if (Head >= LEN_INBUFF) Head = 0;
}

static int16 Poll(void)
{
    int16 Start;
    int16 CmdHead;
    while (LenQueue > 0)
    {
//        DINT;
        Start = HeadQueue - (LenQueue--) * STEP;
//        EINT;
        CmdHead = FindPack(Start);
        if (CmdHead >= 0)
        {
            int16 Cmd = GetCmd(CmdHead);
            if ((StepCnt * STEP + N_FFT) < LEN_PBUFF)
                memset(PackBuff, 0, (StepCnt * STEP + N_FFT) * (sizeof(int16)));
            else
                memset(PackBuff, 0, (LEN_PBUFF) * (sizeof(int16)));
            StepCnt = 0;
            return (Cmd);
        }
    }
    return (-1);
}

static float calc_FRM(int16 data[], int16 n)
{
    int32 Re   = data[0];
    int32 Im   = 0;
    int32 Re_p = 0;
    int32 Im_p = 0;
    int16 i    = 0;
    for (i = 0; i < n; ++i)
    {
        Re_p = Re;
        Im_p = Im;
        Re = ((coef_1) * (Re_p) >> 10) - ((coef_2) * (Im_p) >> 10) + (int32) data[i];
        Im = ((coef_1) * (Im_p) >> 10) + ((coef_2) * (Re_p) >> 10);
    }
    float Im_r = Im / Fs;
    float Re_r = Re / Fs;
    return (Re_r * Re_r + Im_r * Im_r);
}

static float calc_FRM2(int16 data[], int16 n)
{
    int32 Re   = data[0];
    int32 Im   = 0;
    int32 Re_p = 0;
    int32 Im_p = 0;
    int16 i    = 0;
    for (i = 0; i < n; ++i)
    {
        Re_p = Re;
        Im_p = Im;
        Re = ((coef_1) * (Re_p) >> 10) - ((coef_2) * (Im_p) >> 10) + (int32) data[i];
        Im = ((coef_1) * (Im_p) >> 10) + ((coef_2) * (Re_p) >> 10);
    }
    float Im_r = Im / (Fc * n);
    float Re_r = Re / (Fc * n);
    return (2 * Fs * sqrt(Re_r * Re_r + Im_r * Im_r));
}

#if USE_LONG_FRM
#if USE_FLOAT_RMS
static float rms_amplf(int16 data[], int16 n)
{
    int16 i = 0;
    float mean = 0;
    for (i = 0; i < n; ++i) mean += (int32) data[i] * (int32) data[i];
    return (1.414f * sqrtf(mean / n));
}
#else //USE_FLOAT_RMS
static Uint16 rms_amplf(int16 data[], int16 n)
{
    int16 i = 0;
    int32 mean = 0;
    for (i = 0; i < n; ++i) mean += (int32) data[i] * (int32) data[i];
    return (Uint16)(1.414f * sqrtf(mean / n));
}
#endif //USE_FLOAT_RMS

#else //USE_LONG_FRM

#if USE_FLOAT_RMS
static float rms_amplf(int16 data[], int16 n)
{
    int16 i = 0;
    float mean = 0;
    for (i = 0; i < n; ++i) mean += (float) data[i] * (float) data[i];
    return (1.414f * sqrtf(mean / n));
}
#else //USE_FLOAT_RMS
static Uint16 rms_amplf(int16 data[], int16 n)
{
    int16 i = 0;
    float mean = 0;
    for (i = 0; i < n; ++i) mean += (float) data[i] * (float) data[i];
    return (Uint16)(1.414f * sqrtf(mean / n));
}
#endif //USE_FLOAT_RMS
#endif //USE_LONG_FRM

#if SHIFT
static void mas_shift(Uint16 mas[])
{
    Uint16 temp = mas[7];
    int16 i = 8;
    while (--i) mas[i] = mas[i - 1];
    mas[0] = temp;
}
#endif //SHIFT

////////////////

#ifdef _FLASH
	#pragma CODE_SECTION(ARSF_Dekod,  ".TI.ramfunc");
	#pragma CODE_SECTION(ARSF_Demod,  ".TI.ramfunc");
	#pragma CODE_SECTION(ARSF_Update, ".TI.ramfunc");
	#pragma CODE_SECTION(ARSF_Receive,".TI.ramfunc");
	#pragma CODE_SECTION(ARSF_Init,   ".TI.ramfunc");
	#pragma CODE_SECTION(ARSF_Reset,  ".TI.ramfunc");
#endif

void ARSF_Reset()
{
	ARSF_FazeKodAbs=0;
	ARSF_FreqCorrect=0;
	ARSF_LastLevel=0;
	ARSF_FazeDetect=0;

	for (ARSF_indexDemod=0;ARSF_indexDemod<6*8+1;ARSF_indexDemod++) ARSF_KodDemod[ARSF_indexDemod] = 0;
	ARSF_indexDemod=0;
	ARSF_AmplitudaAvgSum=0;
	ARSF_AmplitudaAvgIndex=0;

	ARSF_AmplMax = 0;
	ARSF_AmplMin = 0;

	ARSF_Struct_1.Flags.flag.Reset=0;
}

static unsigned char ARSF_FazeKodAbs2;   // ���������� ����� ���� �� ������
//static char ARSF_FreqCorrect2;           // ���������� �������
//static unsigned char ARSF_LastLevel2;    // ���������� � ���������� ������
//static unsigned char ARSF_FazeDetect2;   // ������� ����������� ���� � �������

static int16 ARSF_AmplMax2, ARSF_AmplMin2;  // ������������ ���������
Uint16 ARSF_Amplituda2;                                      // ��������� ������� �� ������ ������� �� ������ 12��
static Uint32 ARSF_AmplitudaAvgSum2;
static Uint16 ARSF_AmplitudaAvgIndex2;

void ARSF_Reset2()
{
    ARSF_FazeKodAbs2=0;
//    ARSF_FreqCorrect2=0;
//    ARSF_LastLevel2=0;
//    ARSF_FazeDetect2=0;

    ARSF_AmplitudaAvgSum2=0;
    ARSF_AmplitudaAvgIndex2=0;

    ARSF_AmplMax2 = 0;
    ARSF_AmplMin2 = 0;

//    ARSF_Struct.Flags.flag.Reset=0;
}

void ARSF_Init()
{
	ARSF_filter.order=ARSF_filter_v2_ORDER;
	ARSF_filter.dbuffer_ptr=ARSF_filter_dbuffer;
	ARSF_filter.coeff_ptr=(long *)ARSF_filter_coeff;
	ARSF_filter.cbindex = 0;
	ARSF_filter.input = 0;
	ARSF_filter.init = (void (*)(void *))FIR16_init;
	ARSF_filter.calc = (void (*)(void *))FIR16_calc;
	ARSF_filter.init(&ARSF_filter);

	ARSF_Struct.Flags.all = 0;              // ��������� ������, ����������, �����
	ARSF_Struct.Kod = 0;

	ARSF_Struct_1.Flags.all = 0;              // ��������� ������, ����������, �����
	ARSF_Struct_1.Kod = 0;

	ARSF_Struct_2.Flags.all = 0;              // ��������� ������, ����������, �����
	ARSF_Struct_2.Kod = 0;

	ARSF_Reset();
	Version |= FRM_ALGORITM;

	ARSF_CycleWork = ARSF_TIME_NoFRM_Update;
	ARSF_Time_Limit = ARSF_TIME_NoRecieveKod;
	ARSF_Amplituda = 0;
	ARSF_LevelNoise = 15;
	ARSF_LevelDetect = 40;
	ARSF_EndFRM = 20;

	//ARU
#if ARU_CONS
	ARU_coef = ARU_COEF;
#endif
	ARU_update = 1;
//	GpioDataRegs.GPBCLEAR.all = 0x000C;
	//

    memset(InBuff, 0, sizeof(InBuff));
    memset(PackBuff, 0, sizeof(PackBuff));
}

/**
 *  @brief ������������� �������
 *
 *  @param Ns   ����� �������������� �����
 *  @param point  ��������� �����
 *  @return ���
*/

#if ALIKE
int16 ARSF_Dekod(void)
{
    // ������� ������������� ���� ������
    static const unsigned char KodBayera[] = { 0x01, 0x1F, 0x2C, 0x32, 0x4A,
                                               0x54, 0x67, 0x79, 0x86, 0x98,
                                               0xAB, 0xB5, 0xCD, 0xD3, 0xE0,
                                               0xFE };
    unsigned char Low = 0, High = 0;
    unsigned char i, j, k;
    char max = 0, error = 1, search = 0;

    if (ARSF_indexDemod < 16)
        return 0;             // �������� �������
    for (i = 0; i < ARSF_indexDemod - 1; i++)
        ARSF_KodDemod[i] = (4 + ARSF_KodDemod[i + 1] - ARSF_KodDemod[i]) % 4;

    if (!new_pack_find)
    {
        for (i = 0; i < ARSF_indexDemod - 16; i++) // ���������� ���� ���������� ������� �������������������
        {
            for (j = 0; j < 8; j++)
                if (ARSF_KodDemod[i + j] != ARSF_KodDemod[i + 8 + j])
                    break;
            if (j != 8)
                error++;
            else
            {
                max = i;
                for (k = 0; k < 8; k++) // ������������� ������� ��������� � ���
                    switch (ARSF_KodDemod[max + k])
                    {
                    case 0:
                        High = High << 1;
                        Low  = Low  << 1;
                        break;
                    case 1:
                        High = High << 1;
                        Low  = Low  << 1 | 1;
                        break;
                    case 2:
                        High = High << 1 | 1;
                        Low  = Low  << 1 | 1;
                        break;
                    case 3:
                        High = High << 1 | 1;
                        Low  = Low  << 1;
                        break;
                    }
                for (search = 0; search < 8; search++)
                {
                    error = 3;
                    for (k = 0; k < 16; k++) // ������������� �� ���� ������
                    {
                        if (KodBayera[k] == High)
                        {
                            High = k;
                            error &= 0x2;
                        }
                        if (KodBayera[k] == Low)
                        {
                            Low = k;
                            error &= 0x1;
                        }
                    }
                    if (!error)
                        break;
                    High = High << 1;
                    High = ((High & 0x0100) >> 8) | (High & 0x00FF);
                    Low  = Low  << 1;
                    Low  = ((Low  & 0x0100) >> 8) | (Low  & 0x00FF);
                }
                if (!error)
                    break;
            }
        }
        if (error)
            return 0; // �� ��������� �������� ���
        else
            return (High << 4) | Low; // ���
    }
    else
    {
        if (alike_v2(ARSF_KodDemod, ARSF_indexDemod, &max))
        {
            for (k = 0; k < 8; k++) // ������������� ������� ��������� � ���
                switch (ARSF_KodDemod[max + k])
                {
                case 0:
                    High = High << 1;
                    Low  = Low  << 1;
                    break;
                case 1:
                    High = High << 1;
                    Low  = Low  << 1 | 1;
                    break;
                case 2:
                    High = High << 1 | 1;
                    Low  = Low  << 1 | 1;
                    break;
                case 3:
                    High = High << 1 | 1;
                    Low  = Low  << 1;
                    break;
                }

            for (search = 0; search < 8; search++)
            {
                error = 3;
                for (k = 0; k < 16; k++) // ������������� �� ���� ������
                {
                    if (KodBayera[k] == High)
                    {
                        High = k;
                        error &= 0x2;
                    }
                    if (KodBayera[k] == Low)
                    {
                        Low = k;
                        error &= 0x1;
                    }
                }
                if (!error)
                    break;
                High = High << 1;
                High = ((High & 0x0100) >> 8) | (High & 0x00FF);
                Low  = Low  << 1;
                Low  = ((Low  & 0x0100) >> 8) | (Low  & 0x00FF);
            }
        }
        else
            error++;

        if (error)
            return 0; // �� ��������� �������� ���
        else
            return (High << 4) | Low; // ���
    }
}
#else
int16 ARSF_Dekod(void)
{
    // ������� ������������� ���� ������
    static const unsigned char KodBayera[] = {  0x01, 0x1F, 0x2C, 0x32, 0x4A, 0x54, 0x67, 0x79, 0x86, 0x98, 0xAB, 0xB5, 0xCD, 0xD3, 0xE0, 0xFE  };

    unsigned char Low=0, High=0;
    unsigned char i,j,k;
    char max = 0, error = 1, search=0;

    if (ARSF_indexDemod < 16) return 0;             // �������� �������
    for (i = 0; i < ARSF_indexDemod - 1; i++)
        ARSF_KodDemod[i] = ( 4 + ARSF_KodDemod[i+1] - ARSF_KodDemod[i] ) % 4;
    for (i = 0; i < ARSF_indexDemod - 16; i++)             // ���������� ���� ���������� ������� �������������������
    {
        for(j = 0; j < 8; j++)
            if (ARSF_KodDemod[i+j] != ARSF_KodDemod[i+8+j])
                break;
        if (j != 8) error++;
        else
        {
            max = i;

            for(k = 0; k < 8; k++)              // ������������� ������� ��������� � ���
                switch(ARSF_KodDemod[max+k])
                {
                    case 0: High = High<<1;     Low = Low<<1;   break;
                    case 1: High = High<<1;     Low = Low<<1|1; break;
                    case 2: High = High<<1|1;   Low = Low<<1|1; break;
                    case 3: High = High<<1|1;   Low = Low<<1;   break;
                }

            for(search=0; search<8; search++)
            {
                error=3;
                for(k=0;k<16;k++)             // ������������� �� ���� ������
                {
                    if(KodBayera[k]==High) { High=k; error&=0x2; }
                    if(KodBayera[k]==Low)  { Low=k;  error&=0x1; }
                }
                if( !error ) break;
                High = High<<1; High = ((High&0x0100)>>8)|(High&0x00FF);
                Low = Low<<1; Low = ((Low&0x0100)>>8)|(Low&0x00FF);
            }
            if( !error ) break;
        }
    }

    if( error ) return 0;           // �� ��������� �������� ���
    else return (High<<4) | Low;         // ���
}
#endif //ALIKE

void ARSF_Demod(int16 point)
{
    static unsigned char FazeCurrent = 0, FazeCount = 0, NoFazeCount = 0;

    if (point > ARSF_LevelHi )          // ������� �������
    {
        if (ARSF_LastLevel == 2)            // Low -> Hi
        {
            if( ARSF_FazeKodAbs<=2 ) ARSF_FazeDetect = 1;
            else if( 3<=ARSF_FazeKodAbs && ARSF_FazeKodAbs<=7 ) ARSF_FazeDetect = 2;
            else if( 8<=ARSF_FazeKodAbs && ARSF_FazeKodAbs<=12 ) ARSF_FazeDetect = 3;
            else if( 13<=ARSF_FazeKodAbs && ARSF_FazeKodAbs<=17 ) ARSF_FazeDetect = 4;
            else if( 18<=ARSF_FazeKodAbs ) ARSF_FazeDetect = 1;
        }
        ARSF_LastLevel = 1; //Hi
    }
    if( point < ARSF_LevelLow )         // ������ �������
    {
        if (ARSF_LastLevel == 1)            // Hi -> Low
        {
            if( ARSF_FazeKodAbs<=2 ) ARSF_FreqCorrect -= ARSF_FazeKodAbs;
            else if( 3<=ARSF_FazeKodAbs && ARSF_FazeKodAbs<=7 ) ARSF_FreqCorrect += 5-ARSF_FazeKodAbs;
            else if( 8<=ARSF_FazeKodAbs && ARSF_FazeKodAbs<=12 ) ARSF_FreqCorrect += 10-ARSF_FazeKodAbs;
            else if( 13<=ARSF_FazeKodAbs && ARSF_FazeKodAbs<=17 ) ARSF_FreqCorrect += 15-ARSF_FazeKodAbs;
            else if( 18<=ARSF_FazeKodAbs ) ARSF_FreqCorrect += 20-ARSF_FazeKodAbs;
            if( ARSF_FreqCorrect> 8 )
            {   ARSF_FazeKodAbs++; ARSF_FreqCorrect=0;}
            if( ARSF_FreqCorrect<-8 )
            {   ARSF_FazeKodAbs--; ARSF_FreqCorrect=0;}
        }
        ARSF_LastLevel = 2; // Low
    }

    if (point < ARSF_AmplMin) ARSF_AmplMin = point;     // ������� �� ������
    if (point > ARSF_AmplMax) ARSF_AmplMax = point;// �������� �� ������

    ARSF_FazeKodAbs++;// ���������� �������� �� �������� (������������ ��)
    if (ARSF_FazeKodAbs >= 20)// ���������� ������
    {
        ARSF_FazeKodAbs %= 20;

        if (FazeCurrent == ARSF_FazeDetect)
        {
            FazeCount++;
            NoFazeCount = 0;
        }
        else if (NoFazeCount++ && ARSF_indexDemod < (5*8+2))
        {
            if( FazeCount>=5  ) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            if( FazeCount>=22 ) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            if( FazeCount>=38 ) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            if( FazeCount>=54 ) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            if( FazeCount>=70 ) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            if( FazeCount>=86 ) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            if( FazeCount>=102) ARSF_KodDemod[ARSF_indexDemod++] = FazeCurrent;
            FazeCount = 0;
            FazeCurrent=ARSF_FazeDetect;
        }
        ARSF_FazeDetect=0; // ���������� � ����������� ���� �� ��������� �������

        ARSF_AmplitudaAvgSum += (ARSF_AmplMax-ARSF_AmplMin);// ���������� ����. ���������
        ARSF_AmplitudaAvgIndex++;
        ARSF_AmplMax = 0;
        ARSF_AmplMin = 0;
    }
}

Uint16 ARSF_Update(int16 input)
{
	static Uint16 Count_Time_Update = 0;
	static int16 LastPoint=0;
//	int16  Point = 0;
	int16  AbsPoint = 0;
	static Uint16 Count_Detect_Start = 0, Count_Detect_End = 0;
	static int16 noiseAmplMax = 0,  noiseAmplMin = 0,  noiseAmplPeriod = 0, noiseAmplFlag = 1; // ������� ���-��
	static Uint32 noiseAmpl = 0;
	int32  Output;

//	if (use_filter)
//	{
        if (Count_Time_Update++)
        {
        //		// ���������� ������� � ��������� ���
            ARSF_filter.input = input;
            ARSF_filter.calc(&ARSF_filter);
            Point = (ARSF_filter.output + LastPoint) / 2;
            LastPoint = ARSF_filter.output;
            Count_Time_Update=0;
        }
        else
        {
            Point = LastPoint;
        }
//	}
//	else
//	{
//	    Point = input;
//	}

//////////////////
	if (Point < ARSF_AmplMin2) ARSF_AmplMin2 = Point;   // ������� �� ������
    if (Point > ARSF_AmplMax2) ARSF_AmplMax2 = Point;   // �������� �� ������

    ARSF_FazeKodAbs2++;// ���������� �������� �� �������� (������������ ��)
    if (ARSF_FazeKodAbs2 >= 20)// ���������� ������
    {
        ARSF_FazeKodAbs2 %= 20;


        ARSF_AmplitudaAvgSum2 += (ARSF_AmplMax2-ARSF_AmplMin2);// ���������� ����. ���������
        ARSF_AmplitudaAvgIndex2++;
        ARSF_AmplMax2 = 0;
        ARSF_AmplMin2 = 0;
        ARSF_Amplituda = (ARSF_Amplituda + (Uint16)(ARSF_AmplitudaAvgSum/ARSF_AmplitudaAvgIndex))/2;
//        ARSF_Struct_2.Amplituda = ARSF_Amplituda;
    }
/////////////////

    static int16 count_time_update_alg2 = 0;
//    if (count_time_update_alg2++ == 4/*temmp*/)

    if (count_time_update_alg2++ == temmp)
    {
        Receive(thh_2 * Point);
        count_time_update_alg2 = 0;
    }

	AbsPoint = Point < 0 ? -Point : Point;

	if( ARSF_Struct_1.Flags.flag.Start )
	{
		ARSF_Demod(Point);
		if( AbsPoint < ARSF_LevelDetect )
		{
			if( ++Count_Detect_End>20*16 )
			{
				ARSF_Struct_1.Flags.flag.Start = 0;
				Count_Detect_End = 0;
				ARSF_Struct_1.Flags.flag.Receive = 1;
			}
		}
		else if( Count_Detect_End ) Count_Detect_End--;
	}
	else
	{
		if( AbsPoint>ARSF_LevelDetect )
		{
			if( ++Count_Detect_Start>20*5 )
			{
				ARSF_Struct_1.Flags.flag.Start = 1;
				Count_Detect_Start=0;
			}
		}
		else if( Count_Detect_Start ) Count_Detect_Start--;

		if (ARSF_CycleWork < ARSF_TIME_AfterKod)		// ������� ����� ��������� ����, ������ ������ �����
		{
		//	Max		Max
		//   /\      /\			- noiseAmplFlag=1
		//__/  \    /  \    ___
		//  |   \  /|   \  /|
		//  |    \/ |    \/	|	- noiseAmplFlag=0
		//  |	Min	|	Min	|
		//	\		|		/
		//	 \ Update Date /

			if( Point > ARSF_NOISE_AMPL_DETECT  )
			{
				if( !noiseAmplFlag )
				{
					noiseAmplFlag=1;
					noiseAmpl += (noiseAmplMax-noiseAmplMin);
					noiseAmplMax = 0;
					noiseAmplMin = 0;
					noiseAmplPeriod++;
				}
				if( Point > noiseAmplMax ) noiseAmplMax=Point;
			}
			if( Point < -ARSF_NOISE_AMPL_DETECT  )
			{
				if( noiseAmplFlag ) noiseAmplFlag=0;
				if( Point < noiseAmplMin ) noiseAmplMin=Point;
			}
		}
		else if( noiseAmplPeriod )				// ��������� ���������� ������
		{
			noiseAmpl = noiseAmpl/noiseAmplPeriod;
			noiseAmplPeriod = 0;
			noiseAmplFlag=1;
			ARSF_LevelNoise = (ARSF_LevelNoise+noiseAmpl)/2;
			noiseAmpl = 0;
			ARSF_EndFRM = ARSF_LevelNoise/2;
			if( ARSF_EndFRM<10 ) ARSF_EndFRM=10;
			if( ARSF_EndFRM>40 ) ARSF_EndFRM=40;

			if( ARSF_Amplituda>ARSF_LevelNoise ) ARSF_LevelDetect = ARSF_LevelNoise/2 + ARSF_Amplituda/10;
			else
				if( ARSF_Amplituda ) ARSF_LevelDetect = ARSF_Amplituda/3;
				else ARSF_LevelDetect = ARSF_LevelNoise*3/4;

			if( ARSF_LevelDetect<ARSF_LevelDetect_Low ) ARSF_LevelDetect = ARSF_LevelDetect_Low;
			if( ARSF_LevelDetect>ARSF_LevelDetect_Hi  ) ARSF_LevelDetect = ARSF_LevelDetect_Hi;
		}
	}

	if( ARSF_CycleWork++ > ARSF_Time_Limit )		// ��� ������ ��� ����� � ������� ~750��
	{
		ARSF_Struct_1.Kod = 0;						// �� ������ ������ 0 ����������� �� ������
		ARSF_Struct_1.Amplituda_8bit = ARSF_Amplituda = 0;
		if( ARSF_Amplituda>ARSF_LevelNoise ) ARSF_LevelDetect = ARSF_LevelNoise/2 + ARSF_Amplituda/10;
		else
			if( ARSF_Amplituda ) ARSF_LevelDetect = ARSF_Amplituda/3;
			else ARSF_LevelDetect = ARSF_LevelNoise*3/4;
		if( ARSF_LevelDetect<ARSF_LevelDetect_Low ) ARSF_LevelDetect = ARSF_LevelDetect_Low;
		if( ARSF_LevelDetect>ARSF_LevelDetect_Hi  ) ARSF_LevelDetect = ARSF_LevelDetect_Hi;
		if( ARSF_Amplituda>ARSF_Level_ARU_Step_Down ) if( ARU_coef   )	{ ARU_coef--; ARU_update = 1; ARSF_LevelNoise = ARSF_LevelNoise/2; ARSF_Amplituda = ARSF_Amplituda/2; }
		if( ARSF_Amplituda<ARSF_Level_ARU_Step_Up   ) if( ARU_coef<3 ) 	{ ARU_coef++; ARU_update = 1; ARSF_LevelNoise = ARSF_LevelNoise*2; ARSF_Amplituda = ARSF_Amplituda*2; }

		ARSF_CycleWork = 0;
		ARSF_Time_Limit = ARSF_TIME_NoFRM_Update;
		ARSF_Struct_1.Flags.flag.New_Update = 0;
	}

	if (ARU_update)
	{
	    GPIO_WritePin(KUO_GPIO, 0);
	    GPIO_WritePin(KU1_GPIO, 0);
#if ARU_CONST
		ARU_coef = ARU_COEF;
#endif //ARU_CONST
		switch (ARU_coef)
		{
			case 0: break;
#if ARU_INV
			case 1: GpioDataRegs.GPBSET.bit.GPIOB3 = 1; break;
			case 2: GpioDataRegs.GPBSET.bit.GPIOB2 = 1; break;
			case 3: GpioDataRegs.GPBSET.all = 0x000C;   break;
#else
			case 1: GPIO_WritePin(KUO_GPIO, 1);
	        GPIO_WritePin(KU1_GPIO, 0); break;
			case 2: GPIO_WritePin(KUO_GPIO, 0);
	        GPIO_WritePin(KU1_GPIO, 1); break;
			case 3: GPIO_WritePin(KUO_GPIO, 1);
	        GPIO_WritePin(KU1_GPIO, 1);   break;
#endif //ARU_INV
		}
		ARU_update = 0;
	}

	return (unsigned int)Output;
}


void ARSF_Receive()
{
	static Uint8 Amplituda_8bit_p = 0;

	if (!Can.Pdo1_M.Command2.bit.Change_sens)
	{
	    alg_sens = 0;

        if (ARSF_Struct_1.Kod && ARSF_Struct_1.Amplituda_8bit)
        {
            ARSF_Struct.Kod = ARSF_Struct_1.Kod;
            ARSF_Struct.Amplituda_8bit = ARSF_Struct_1.Amplituda_8bit;
            ARSF_Struct.Flags.flag.Receive = 1;
            ARSF_Struct_1.Flags.flag.New_Update = 1;
            Can.Pdo4_S.FRM.bit.Algorithm1 = 1;
            if (ARSF_Struct_2.Kod && ARSF_Struct_2.Amplituda_8bit > 20)
            {
                Can.Pdo4_S.FRM.bit.Algorithm2 = 1;
            }
            Amplituda_8bit_p = ARSF_Struct_1.Amplituda_8bit;
        }
        else if (ARSF_Struct_2.Kod && ARSF_Struct_2.Amplituda_8bit > 20)
        {
            ARSF_Struct.Kod = ARSF_Struct_2.Kod;
            ARSF_Struct.Amplituda_8bit = (ARSF_Struct_1.Amplituda_8bit > 0) ? ARSF_Struct.Amplituda_8bit : Amplituda_8bit_p;
            ARSF_Struct.Flags.flag.Receive = 1;
            ARSF_Struct_2.Flags.flag.New_Update = 1;
            Can.Pdo4_S.FRM.bit.Algorithm1 = 0;
            Can.Pdo4_S.FRM.bit.Algorithm2 = 1;
        }
        else
        {
            ARSF_Struct.Kod = 0;
            ARSF_Struct.Amplituda_8bit = 0;
            Can.Pdo4_S.FRM.bit.Algorithm1 = 0;
            Can.Pdo4_S.FRM.bit.Algorithm2 = 0;
        }
	}
	else
	{
        alg_sens = 1;

	    if (ARSF_Struct_1.Kod && ARSF_Struct_1.Amplituda_8bit) {
            ARSF_Struct.Kod = ARSF_Struct_1.Kod;
            ARSF_Struct.Amplituda_8bit = ARSF_Struct_1.Amplituda_8bit;
            ARSF_Struct.Flags.flag.Receive = 1;
            ARSF_Struct_1.Flags.flag.New_Update = 1;
            if (ARSF_Struct_2.Kod && ARSF_Struct_2.Amplituda_8bit > 10)
            {
                Can.Pdo4_S.FRM.bit.Algorithm2 = 1;
            }
            Amplituda_8bit_p = ARSF_Struct_1.Amplituda_8bit;
        } else if (ARSF_Struct_2.Kod && ARSF_Struct_2.Amplituda_8bit > 10) {
            ARSF_Struct.Kod = ARSF_Struct_2.Kod;
            ARSF_Struct.Amplituda_8bit = (ARSF_Struct_1.Amplituda_8bit > 0) ? ARSF_Struct.Amplituda_8bit : Amplituda_8bit_p;
            ARSF_Struct.Flags.flag.Receive = 1;
            ARSF_Struct_2.Flags.flag.New_Update = 1;
            Can.Pdo4_S.FRM.bit.Algorithm1 = 0;
            Can.Pdo4_S.FRM.bit.Algorithm2 = 1;
        } else {
            ARSF_Struct.Kod = 0;
            ARSF_Struct.Amplituda_8bit = 0;
            Can.Pdo4_S.FRM.bit.Algorithm1 = 0;
            Can.Pdo4_S.FRM.bit.Algorithm2 = 0;
        }
	}

    if (ARSF_Struct_1.Flags.flag.New_Update || ARSF_Struct_2.Flags.flag.New_Update)
    {
        ARSF_Struct.Flags.flag.New_Update = 1;
    }

    if (!ARSF_Struct_1.Flags.flag.New_Update && !ARSF_Struct_2.Flags.flag.New_Update)
    {
        ARSF_Struct.Flags.flag.New_Update = 0;
    }

    thh_2 = (ARSF_Struct_1.Amplituda_8bit < 5) ? 100 : 10;

	ARSF_Struct.Amplituda_Fc_8bit = ARSF_Struct_1.Amplituda_8bit;

    static int16 j = 0;
    if (!is_buffer_aft_fil_full)
    {
        if (j < N_RMS)
            buffer_aft_fil[j++] = Point;
        else
            j = 0, is_buffer_aft_fil_full = 1;
    }
    static int16 i_rms = 0;
    static float max_rms_ampl = 0;
    if (thh_calc)
    {
        if (is_buffer_aft_fil_full == 1)
        {
            rms_ampl = rms_amplf(buffer_aft_fil, N_RMS);
            is_buffer_aft_fil_full = 0;
            if (rms_ampl > max_rms_ampl)
                max_rms_ampl = rms_ampl;
            i_rms++;
            if (i_rms == 20)
            {
                if (max_rms_ampl < th1)
                    thh_2 = 100;
                if (max_rms_ampl > th2)
                    thh_2 = 10;
                i_rms = 0;
                max_rms_ampl = 0;
            }
        }
    }

	Uint16 AmplKod;

	if( ARSF_Struct_1.Flags.flag.Receive )        // ���������� �� ���������� ��������� ���������� ������
	{
		ARSF_Struct_1.Kod = ARSF_Dekod();           // ������������� ������� ��������� � ���
		ARSF_Amplituda = (ARSF_Amplituda + (Uint16)(ARSF_AmplitudaAvgSum/ARSF_AmplitudaAvgIndex))/2;			// ���������

//		// 23.09.19
#if AMP_FC
        AmplKod = ARSF_Amplituda >> (3 + ARU_coef);
        if (AmplKod > 255) AmplKod = 255;
		ARSF_Struct_1.Amplituda_8bit = AmplKod;
#endif
//		//// 23.09.19

		if(ARSF_Struct_1.Kod)
		{
			ARSF_CycleWork = 0;
			ARSF_Time_Limit = ARSF_TIME_NoRecieveKod;
			ARSF_Struct_1.Flags.flag.New_Update = 1;
//			 230919
#if !AMP_FC
	        AmplKod = ARSF_Amplituda >> (3);
//            AmplKod = ARSF_Amplituda >> (3 + ARU_coef);
	        if (AmplKod > 255) AmplKod = 255;
	        ARSF_Struct_1.Amplituda_8bit = AmplKod;
#endif
			// 230919

			if( ARSF_Amplituda>ARSF_Level_ARU_Step_Down ) if( ARU_coef   )	{ ARU_coef--; ARU_update = 1; ARSF_LevelNoise = ARSF_LevelNoise/2; ARSF_Amplituda = ARSF_Amplituda/2; }
			if( ARSF_Amplituda<ARSF_Level_ARU_Step_Up   ) if( ARU_coef<3 ) 	{ ARU_coef++; ARU_update = 1; ARSF_LevelNoise = ARSF_LevelNoise*2; ARSF_Amplituda = ARSF_Amplituda*2; }
		}
		else
		{
			if( ARSF_Amplituda>ARSF_LevelNoise ) ARSF_LevelDetect = ARSF_LevelNoise/2 + ARSF_Amplituda/10;
			else
				if( ARSF_Amplituda ) ARSF_LevelDetect = ARSF_Amplituda/3;
				else ARSF_LevelDetect = ARSF_LevelNoise*3/4;
			/*if( ARSF_Amplituda>ARSF_LevelNoise ) ARSF_LevelDetect = ARSF_LevelNoise + ARSF_Amplituda/6;
			else
				if( ARSF_Amplituda ) ARSF_LevelDetect = ARSF_Amplituda*2/3;
				else ARSF_LevelDetect = ARSF_LevelDetect*3/4;*/

			if( ARSF_LevelDetect<ARSF_LevelDetect_Low ) ARSF_LevelDetect = ARSF_LevelDetect_Low;
			if( ARSF_LevelDetect>ARSF_LevelDetect_Hi  ) ARSF_LevelDetect = ARSF_LevelDetect_Hi;
		}

		ARSF_Reset();
	    ARSF_Struct_1.Flags.flag.Receive = 0;       // ���������� ��������� ���������
	}
    //////////////
//    if (clac_alg2)
//    {

#if FC_ALL_TIME
        static int16 iii = 0;
        static Uint32 mean_maxFc = 0;
//    static Uint32 Amplituda_Fc_8bit = 0;
        iii++;
//    mean_maxFc += maxFc;
//    if (iii == 8)
//    {
////        Amplituda_Fc_8bit = (mean_maxFc / 8) >> 8;
//        iii = 0;
//        mean_maxFc = 0;
//    }
        if (maxFc > mean_maxFc) mean_maxFc = maxFc;
        if (iii == 10)
        {
            Amplituda_Fc_8bit = (mean_maxFc) >> 4;
            iii = 0;
            mean_maxFc = 0;
        }

#endif //FC_ALL_TIME

        int16 Cmd = Poll();

#if SENS
        if (Cmd > -1 && Validity == 1)
        {
            if (Cmd == Cmd3)
            {
                ARSF_Struct_2.Kod = Cmd;
                ARSF_Struct_2.Flags.flag.Receive = 1;
                ARSF_Struct_2.Flags.flag.New_Update = 1;
//            ARSF_Struct.Kod = Cmd;
//			ARSF_Struct.Flags.flag.Receive = 1;
//			ARSF_Struct.Flags.flag.New_Update = 1;
                ARSF_Struct.Amplituda_8bit = Validity * 10;
#if !SNR_CALC
                ARSF_Struct_2.Amplituda_Fc_8bit = ampl;
#else
#if FC_ALL_TIME
                ARSF_Struct_2.Amplituda_Fc_8bit = Amplituda_Fc_8bit;
#else
                ARSF_Struct_2.Amplituda_Fc_8bit = 0;
#endif //FC_ALL_TIME
#endif //SNR_CALC

//            ARSF_Struct.Amplituda_Fc_8bit = maxFc >> 4;
//            ARSF_Struct.Amplituda_8bit = rms_ampl;
                ARSF_CycleWork_2 = 0;
            }
            Cmd2 = Cmd;
        }
#endif //SENS
        if (Cmd > -1 && Validity > 1)
        {
#if FALSE_OPERATION
            frm_count++;
            ARSF_CycleWork_frm_count = 0;
#endif //FALSE_OPERATION
#if SENS
            Cmd3 = Cmd;
#endif //SENS
            ARSF_Struct_2.Kod = Cmd;
            ARSF_Struct_2.Flags.flag.Receive = 1;
            ARSF_Struct_2.Flags.flag.New_Update = 1;
            ARSF_Struct_2.Amplituda_8bit = Validity * 10;
#if !SNR_CALC
            ARSF_Struct_2.Amplituda_Fc_8bit = ampl;
#else
#if FC_ALL_TIME
            ARSF_Struct_2.Amplituda_Fc_8bit = Amplituda_Fc_8bit;
#else
            ARSF_Struct_2.Amplituda_Fc_8bit = 0;
#endif //FC_ALL_TIME
#endif //SNR_CALC

//        ARSF_Struct.Amplituda_Fc_8bit = maxFc >> 4; //ARSF_Struct.Amplituda_Fc_8bit = 0;
//        ARSF_Struct.Amplituda_8bit = rms_ampl;
            ARSF_CycleWork_2 = 0;
        }
#if FALSE_OPERATION
        if (ARSF_CycleWork_frm_count++ > ARSF_Time_Limit_frm_count)
            frm_count = 0;
#endif //FALSE_OPERATION
        if (ARSF_CycleWork_2++ > ARSF_Time_Limit_2)
        {
            ARSF_Struct_2.Kod = 0;
            ARSF_Struct_2.Amplituda_8bit = 0;
            ARSF_Struct_2.Flags.flag.Reset = 0; //reset
            ARSF_Struct_2.Flags.flag.Receive = 0;
            ARSF_Struct_2.Flags.flag.New_Update = 0;
        }
//    }
}

static int16 CmdDetect2(int16 aCmdHead, Uint16 delta)
{
    Uint16 i, j, k, Tr;
    Uint16 phTrans[8];
    Uint8  b1 = 0, b2 = 0;
    for (k = 0; k < 8; k++)
    {
        Uint16 o1 = aCmdHead + (k - 1) * LEN_BURST + delta;
        int32 Max = 0x80000000;
        for (i = 0; i < 4; i++)
        {
            Uint16 o2 = o1 + LEN_BURST + i * (T / 4);
            int32 Sum = 0;
            for (j = BORD; j < LEN_BURST - BORD - T; j++)
            {
                Sum += ((int32) PackBuff[o1 + j] * (int32) PackBuff[o2 + j]) >> 14;
            }
            if (Sum > Max)
            {
                phTrans[k] = i;
                Max = Sum;
            }
        }
    }

    Uint16 b1_mas[8];
    Uint16 b2_mas[8];

    for (i = 0; i < 8; i++)
    {
        Tr = phTrans[i];
        if /**/ (Tr == 2) Tr = 3;
        else if (Tr == 3) Tr = 2;

        b1_mas[i] = (Tr >>  1);
        b2_mas[i] = (Tr & 0x1);

        b1 = (b1 << 1) + (Tr >>  1);
        b2 = (b2 << 1) + (Tr & 0x1);
    }

    Uint16 b1_mas_sum = 0;
    Uint16 b2_mas_sum = 0;

    for (i = 0; i < 4; i++)
    {
        b1_mas_sum += b1_mas[i];
        b2_mas_sum += b2_mas[i];
    }

    if (!(b1_mas_sum % 2))
    {
        for (i = 0; i < 3; i++)
        {
            if (b1_mas[i] != b1_mas[i + 4])
            {
                return (-1);
            }
        }
        if (b1_mas[3] != !b1_mas[7])
        {
            return (-1);
        }
    }
    else
    {
        for (i = 0; i < 3; i++)
        {
            if (b1_mas[i] != !b1_mas[i + 4])
            {
                return (-1);
            }
        }
        if (b1_mas[3] != b1_mas[7])
        {
            return (-1);
        }
    }

    if (!(b2_mas_sum % 2))
    {
        for (i = 0; i < 3; i++)
        {
            if (b2_mas[i] != b2_mas[i + 4])
            {
                return (-1);
            }
        }
        if (b2_mas[3] != !b2_mas[7])
        {
            return (-1);
        }
    }
    else
    {
        for (i = 0; i < 3; i++)
        {
            if (b2_mas[i] != !b2_mas[i + 4])
            {
                return (-1);
            }
        }
        if (b2_mas[3] != b2_mas[7])
        {
            return (-1);
        }
    }

    b1 = Bauer(b1);
    b2 = Bauer(b2);
#if SHIFT
    for (j = 0; j < 8; j++)
    {
        if ((b1 == 0xFF) || (b2 == 0xFF))
        {
            b1 = 0;
            b2 = 0;
            mas_shift(phTrans);
            for (i = 0; i < 8; i++)
            {
                Tr = phTrans[i];
                if /**/ (Tr == 2) Tr = 3;
                else if (Tr == 3) Tr = 2;
                b1 = (b1 << 1) + (Tr >>  1);
                b2 = (b2 << 1) + (Tr & 0x1);
            }
            b1 = Bauer(b1);
            b2 = Bauer(b2);
        }
        else break;
    }
#endif
    if (!((b1 == 0xFF) || (b2 == 0xFF))) return ((b1 << 4) + b2);
    else return (-1);
}

#if ALIKE
static int16 alike_v2(Uint8 *ph_mas, Uint8 n_ph_mas, int8 *indx)
{
	int16 i = 0;
	int16 j = 0;
	int16 k = 0;
	int16 m = 0;
	int16 count = 0;
	for (i = 0; i < n_ph_mas - 8; ++i)
	{
		while (m < n_ph_mas)
		{
			for (k = 0; k < (ceilf((n_ph_mas - m) / 8) - 1); ++k)
			{
				count = 0;
				for (j = 0; j < 8; ++j)
				{
					count++;
					if (ph_mas[m + i + j] != ph_mas[m + i + j + (k + 1) * 8])
					{
						//if (j > 0) i += 8;
						break;
					}
				}
				if (count == 8)
				{
					*indx = m + i + (k + 1) * 8;
					return (1);
				}
			}
			m += 8;
		}
	}
	return (0);
}
#endif //ALIKE

//float filterCalc(float input, float b[], float a[])
//{
//    float output = 0;
//
//    static float x[N_FTR] = { 0 };
//    static float y[N_FTR] = { 0 };
//
//    x[0] = input;
//    int i;
//    for (i = 0; i < N_FTR; i++)
//    {
////        output += a[i] * x[i] - b[i] * y[i];
////        output += -a[i] * x[i] - b[i] * y[i];
//        output += a[i] * x[i] + b[i] * y[i];
//    }
//    y[0] = output;
//    for (i = N_FTR - 1; i > 0; i--)
//    {
//        x[i] = x[i - 1];
//        y[i] = y[i - 1];
//    }
//    return output;
//}
//#endif //VER_FRM
//#endif

