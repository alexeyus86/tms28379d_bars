#ifndef CAN_H
#define CAN_H
#include "Defines_FM.h"

//#define VER_CAN	5
//v1: v0 + PDO5 (mailbox 28) + PDO6 (mailbox 29); ����� �������� mailbox.
//v2: v1 + version (mailbox 15 - ����, mailbox 30 - �����)
//v3: v2 + SYNC_OFF + JUMP_CHK (mailbox 31)
//v4: v3 + new nodeguard (mailbox 11 - ����, mailbox 23 - �����)

//	������� ��������� ������� CAN

struct CAN_Status_BITS
{
	Uint16 Receive_NMT:1; // ������ ����� NMT
	Uint16 Receive_SYNC:1; // ������ ����� SYNC
	Uint16 Receive_NodeGuard:1; // ������ ����� NodeGuard
	Uint16 Receive_PDO1:1; // ������ ����� PDO1
	Uint16 Receive_PDO7:1; // ������ ����� PDO7

	Uint16 Slave_to_Work_Mode:1; // ������������ Slave ���� � ������� �����
	Uint16 Reset:1; // ����� ������� ����
	Uint16 Reset_CAN:1; // ����� CAN ����
	
	Uint16 Send_PDO1:1; // �������� ������ PDO1
	Uint16 Send_PDO2:1; // �������� ������ PDO2
	Uint16 Send_PDO3:1; // �������� ������ PDO3
	Uint16 Send_PDO4:1; // �������� ������ PDO4
	Uint16 Send_PDO5:1; // �������� ������ PDO5
	Uint16 Send_PDO6:1; // �������� ������ PDO6
	Uint16 Send_PDO8:1; // �������� ������ PDO8
#ifdef JUMP_CHK
	Uint16 Send_PDO9:1; // �������� ������ PDO9
#endif //JUMP_CHK
	Uint16 Send_NodeGuard:1; // ����� �� ����� NodeGuard ��������

	Uint16 CAN_Error:3; // ���������� ������ �� CAN
	Uint16 Event_10Hz:1; // 100�� �������
};

//  ������� ������� CAN

union CAN_Status_REG
{
   Uint16 all;
   struct CAN_Status_BITS bit;
};

//  ��������� - 4 ����� = ������� �����

struct S_DWORD
{
	Uint16 Byte1:8;
	Uint16 Byte2:8;
	Uint16 Byte3:8;
	Uint16 Byte4:8;	
};

//  ��������� - 2 ����� = �����

struct S_WORD
{
	Uint16 Byte1:8;
	Uint16 Byte2:8;
};

//  ����������� - 2 ����� & �����

union S_WREG
{
   Uint16 word;
   struct S_WORD byte;
};

//  ����������� - 4 ����� & ������� �����

union S_DWREG
{
   long dword;
   struct S_DWORD byte;
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO1 (Master)----------------------------------------*/

//  ������� ��������� ������ ������ PDO1 (Master)
 
struct PDO1_M_BITS
{
    Uint16 Command_Clear_S:1; // �������� ������� ���� S
    Uint16 Command_DKP_Off:1; // ��������� ���
    Uint16 Command_DKP_On:1; // �������� ���
//    Uint16 rsvd:13;
    Uint16 Change_sens:1;
    Uint16 rsvd:12;
};

//  ������� ������ ������ PDO1 (Master)

union PDO1_M_REG
{
   Uint16 all;
   struct PDO1_M_BITS bit;
};

//  ����� ������ PDO1 (Master)

struct PDO1_M
{
    Uint16 Command1;
    union PDO1_M_REG Command2;
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO7 (Master)----------------------------------------*/

//  ������� ��������� ������ ������ PDO7 (Master)

struct PDO7_M
{
    union S_DWREG Version;
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO1 (Slave)-----------------------------------------*/

//  ������� ��������� ������ ������ PDO1 (Slave)

struct PDO1_S_BITS
{
	Uint16 FM_Error:3; // ������������� ������ ������ ��
	Uint16 FM_Update:1; // ���������� ������ �� ��
	Uint16 FM_Prot:1;
	Uint16 Algorithm1:1;
	Uint16 Algorithm2:1;

#ifdef CAN_v1
	Uint16 FRM_Error:3; // ������������� ������ ������ ���
	Uint16 FRM_Update:1; // ���������� ������ �� ���
	Uint16 rsvd:8;
#endif

#ifdef CAN_v2
	Uint16 rsvd:9;
#endif
};

//  ������� ������ ������ PDO1 (Slave)

union PDO1_S_REG
{
   Uint16 all;
   struct PDO1_S_BITS bit;
};

//  ����� ������ PDO1 (Slave)

struct PDO1_S
{
#ifdef CAN_v1
	union PDO1_S_REG FM_FRM;
	Uint16 FM_Kod:8; // ��� ��
	Uint16 FM_Ampl:8; // ��������� ��
	Uint16 FRM_Kod:8; // ��� ���
	Uint16 FRM_Ampl:8; // ��������� ���
#endif
#ifdef CAN_v2
	union PDO1_S_REG FM;
	Uint16 FM_Ampl_80:8; // ��������� �� V���=80
	Uint16 FM_Ampl_70:8; // ��������� �� V���=70
	Uint16 FM_Ampl_60:8; // ��������� �� V���=60
	Uint16 FM_Ampl_40:8; // ��������� �� V���=40
	Uint16 FM_Ampl_00:8; // ��������� �� V���=0
	Uint16 FM_Ampl_RD:8; // ��������� �� V���=��
#endif
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO2 (Slave)-----------------------------------------*/

//  ������� ��������� ������ ������ PDO2 (Slave)

struct PDO2_S_BITS
{
	Uint16 RDS_Derect:1; // ����������� ��������
	Uint16 RDS_True_V:1; // ������������� V� �� ���
	Uint16 RDS_Update_V:1; // ���������� V� ���
	Uint16 RDS_True_S:1; // ������������� S �� ���
	Uint16 RDS_Update_S:1; // ���������� S ���
	Uint16 RDC_V_X10:1; // ����-� ��������� �������� V�
	Uint16 RDS_Err_Lost:1;
	Uint16 RDS_Err_Abort:1;
	Uint16 rsvd:8;
};

//  ������� ������ ������ PDO2 (Slave)

union PDO2_S_REG
{
   Uint16 all;
   struct PDO2_S_BITS bit;
};


//  ����� ������ PDO2 (Slave)
 
struct PDO2_S
{
	union PDO2_S_REG RDS;
	union S_WREG V; // ����������� ��������
	union S_DWREG S; // ���������� ����
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO3 (Slave)-----------------------------------------*/

// ������� ��������� ������ ������ PDO3 (Slave)

struct PDO3_S_BITS
{
	Uint16 RDS_Derect:1; // ����������� ��������
	Uint16 RDS_True_S:1; // ������������� S �� ���
	Uint16 RDS_Update_S:1; // ���������� S ���
	Uint16 isNewSensorRDS:1; // ����� ������ RDS
	Uint16 DKP_OnOff:1; // ��� �������/��������
	Uint16 DKP_Error:1; // ������������� ���
	Uint16 DKP_Ready:1; // ���������� ������ �� ���
	Uint16 isNewSensorRDS2:1; // ����� ������ RDS2
	Uint16 rsvd2:8;
};

// ������� ������ ������ PDO3 (Slave)

union PDO3_S_REG
{
   Uint16 all;
   struct PDO3_S_BITS bit;
};

union PDO3_S_ACCEL
{
	short value;
	struct S_WORD byte;	
};

// ����� ������ PDO3 (Slave)

struct PDO3_S
{
	union PDO3_S_REG DKP;
	union S_DWREG S_dkp; // ���� ��� �������� ����� ���
	union PDO3_S_ACCEL accel;
	Uint16 countAccel;
};

/*---------------------------------------------------------------------------------------------------*/

/*-----------------------------------------PDO4 (Slave)----------------------------------------------*/

//  ������� ��������� ������ ������ PDO4 (Slave)

struct PDO4_S_BITS
{
	Uint16 FRM_Error:3; // ������������� ������ ������ ���
	Uint16 FRM_Update:1; // ���������� ������ �� ���
	Uint16 Algorithm1:1;
	Uint16 Algorithm2:1;
	Uint16 Algorithm3:1;
	Uint16 rsvd:9;
};

//  ������� ������ ������ PDO4 (Slave)

union PDO4_S_REG
{
   Uint16 all;
   struct PDO4_S_BITS bit;
};

//  ����� ������ PDO4 (Slave)

struct PDO4_S
{
	union PDO4_S_REG FRM;
	Uint16 FRM_Kod:8; // ��� ���
	Uint16 FRM_Ampl:8; // ��������� ���
	Uint16 FRM_Ampl_Fc:8; // ��������� �������
	int32 dPeriod;};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO5 (Slave)-----------------------------------------*/

//  ������� ��������� ������ ������ PDO5 (Slave)

struct PDO5_S_BITS
{
	Uint16 RDS_Derect:1; // ����������� ��������
	Uint16 RDS_True_V:1; // ������������� V� �� ���
	Uint16 RDS_Update_V:1; // ���������� V� ���
	Uint16 RDS_True_S:1; // ������������� S �� ���
	Uint16 RDS_Update_S:1; // ���������� S ���
	Uint16 RDC_V_X10:1; // ����-� ��������� �������� V�
	Uint16 RDS_Err_Lost:1;
	Uint16 RDS_Err_Abort:1;
	Uint16 rsvd:8;
};

//  ������� ������ ������ PDO5 (Slave)

union PDO5_S_REG
{
   Uint16 all;
   struct PDO5_S_BITS bit;
};

//  ����� ������ PDO5 (Slave)

struct PDO5_S
{
	union PDO5_S_REG RDS;
	union S_WREG V; // ����������� ��������
	union S_DWREG S; // ���������� ����
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO6 (Slave)-----------------------------------------*/

//	������� ��������� ������ ������ PDO6 (Slave)

struct PDO6_S_BITS
{
	Uint16 RDS_Derect:1; // ����������� ��������
	Uint16 RDS_True_S:1; // ������������� S �� ���
	Uint16 RDS_Update_S:1; // ���������� S ���
	Uint16 isNewSensorRDS:1; // ����� ������ RDS
	Uint16 DKP_OnOff:1; // ��� �������/��������
	Uint16 DKP_Error:1; // ������������� ���
	Uint16 DKP_Ready:1; // ���������� ������ �� ���
	Uint16 rsvd2:9;	
};

//  ������� ������ ������ PDO6 (Slave)

union PDO6_S_REG
{
   Uint16 all;
   struct PDO6_S_BITS bit;
};

union PDO6_S_ACCEL
{
	short  value;
	struct S_WORD byte;	
};

//  ����� ������ PDO6 (Slave)

struct PDO6_S
{
	union PDO6_S_REG DKP;
	union S_DWREG S_dkp; // ���� ��� �������� ����� ���
	union PDO6_S_ACCEL accel;
	Uint16 countAccel;
};

/*---------------------------------------------------------------------------------------------------*/

/*----------------------------------------------PDO8 (Slave)-----------------------------------------*/

//  ����� ������ PDO8 (Slave)

struct PDO8_S
{
    union S_DWREG Version;
};

/*---------------------------------------------------------------------------------------------------*/
#ifdef JUMP_CHK
/*----------------------------------------------PDO9 (Slave)-----------------------------------------*/

//  ������� ��������� ������ ������ PDO9 (Slave)

struct PDO9_S_BITS
{
    Uint16 RDS_Derect:1; // ����������� ��������
    Uint16 RDS_True_S:1; // ������������� S �� ���
    Uint16 RDS_Update_S:1; // ���������� S ���
    Uint16 rsvd:1;
    Uint16 DKP_OnOff:1; // ��� �������/��������
    Uint16 DKP_Error:1; // ������������� ���
    Uint16 DKP_Ready:1; // ���������� ������ �� ���
    Uint16 rsvd2:9;
};

//  ������� ������ ������ PDO6 (Slave)

union PDO9_S_REG
{
   Uint16 all;
   struct PDO9_S_BITS bit;
};

union PDO9_S_ACCEL
{
    short  value;
    struct S_WORD byte;
};

//  ����� ������ PDO6 (Slave)

struct PDO9_S
{
    union PDO9_S_REG DKP;
    union S_DWREG S_dkp; // ���� ��� �������� ����� ���
    union PDO9_S_ACCEL accel;
    Uint16 countAccel;
};

/*---------------------------------------------------------------------------------------------------*/
#endif //JUMP_CHK

//  ��������� ������ �� CAN

struct CAN
{
	union CAN_Status_REG CAN_Status; // ������
	struct PDO1_M Pdo1_M; // ����� ������ PDO1 (Master)
	struct PDO1_S Pdo1_S; // ����� ������ PDO1 (Slave)
	struct PDO2_S Pdo2_S; // ����� ������ PDO2 (Slave)
	struct PDO3_S Pdo3_S; // ����� ������ PDO3 (Slave)
	struct PDO4_S Pdo4_S; // ����� ������ PDO4 (Slave)
	struct PDO5_S Pdo5_S; // ����� ������ PDO5 (Slave)
	struct PDO6_S Pdo6_S; // ����� ������ PDO6 (Slave)
	struct PDO7_M Pdo7_M; // ����� ������ PDO7 (Master)
	struct PDO8_S Pdo8_S; // ����� ������ PDO8 (Slave)
#ifdef JUMP_CHK
    struct PDO9_S Pdo9_S; // ����� ������ PDO9 (Slave)
#endif //JUMP_CHK
};

extern struct CAN Can; // ��������� ������ �� CAN
extern Uint16 Number_FFK; // ���������� ����� �����

void InitCAN(void); // ������������� ������ �� CAN
void UpdateCAN(void); // ������ �������� ������ �� CAN

#endif
