/**
 * @file			FRM_defines.h
 * @brief			������������ ���� � ������������� ��� ���������
 *
 *
 * @author			������ �������
 * @par E-mail:		isakovas@nm.ru
 * @par Copyright:	(c) NII TM, 2006
 */

#ifndef FRM_H
#define FRM_H

//#define N_FFT           256
////#define N_FFT           128

/**
 * @brief  ������� ���������
 */
struct FRM_FLAG {
	Uint16  Analiz:1;		///< ��������� ��������� ������
	Uint16  Start:1;		///< ������� ������ �������
	Uint16  Receive:1;		///< ������� ������� ���������
	Uint16  Complete:1;		///< ������ ����������, ��� �����������
	Uint16  Reset:1;		///< ���������� ������	
	Uint16  KRZ_kod:1;		///< ��� ������� ������, �������� ����������������� ���
	Uint16  Kod_Set:1;		///< �������� ���� �� ������������� ������ �����
	Uint16  Kod_ARU:1;		///< �������� ��������� �� ������������� ������ �����
	Uint16	Refresh_OK:1;	///< ������� ������ ��� ��������� ������ 350��
	Uint16	Channel_Busy:1;	///< ������� ��������� �� ��� �����
	Uint16  Count_Update:4;	///< ������� ��������� �������� ����������� ������
	Uint16  New_Update:1;	///< ������� ������ ����������� ������
	Uint16  rsvd:1;			///< ������
};

union FRM_FLAGS {
   Uint16           all;
   struct FRM_FLAG flag;
};

struct FRM {
	union FRM_FLAGS Flags;	// ������� ���������
	Uint16 Index;
	Uint8  Kod;
	Uint8  Amplituda_8bit;	// ��������� �������
	Uint8  Amplituda_Fc_8bit;  // ��������� �������
	Uint16 Amplituda;		// ��������� �������
	Uint16 KodFaze[4];		// ���� ������� � �������� 16�� ��������
	Uint16 LastKod;
	 int16 NsCorrect;		// ��������� ������� ������ ���������� ��������� �������
	Uint16 Freq;			// ������� ��
	//Uint16 Noise;			// ��������� ����
};

//extern volatile struct FRM Frm;

#endif
