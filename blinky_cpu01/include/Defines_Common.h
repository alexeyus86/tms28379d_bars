#ifndef COMMON_H
#define COMMON_H
#include "F28x_Project.h"

#define KUO_GPIO 86
#define KU1_GPIO 87

//#define	FLASH		1 //Flash-����������

//#define TEST_TMS_TIME 1

//#define TRAIN722 1
//#define TRAIN740 1

//#define DEBUG_50HZ_300HZ	1

//#define DEBUG       1
//#define OLD         1

#define BARS        0

#define PUAV        0

#define CODE2AMPL   1

#define MCP			1

#define CAN_v2		1

#define FRM_ALGO	2

#define ARS         0
#define MAXIM       1
#define SYNC_OFF	1
#define VERSION		1
//#define JUMP_CHK	  1

#if DEBUG
//#define DEBUG_NODEGUARD	1 // flashing of LED
#define DEBUG_DKP	0 // DKP's always power on
#endif //DEBUG

//#define FRM_ALGO_v1	1
//#define FRM_ALGO_v2	1
//#define FRM_ALGO_v3	1

#ifdef TRAIN722
#define RDC_V_x10	1
#elif TRAIN740
#define RDC_V_x10	1
#else
#endif //TRAIN722

#define RDC_Signal_Quadro	1

#ifdef CAN_v1
#define FM_ALGO_v1		1
#define FM_OLD_LEVEL 	1
#define FM_NEW_LEVEL 	1
#endif

#ifdef CAN_v2
#define FM_ALGO_v2		1
#endif

#ifdef FRM_ALGO_v1
//	#define FILTER_IIR	1
#define FILTER_FIR	1
#endif

extern Uint16 Version;

#if FRM_ALGO == 1
#define FRM_ALGO_v1     1

#elif FRM_ALGO == 2
//#define VER_FRM         0
//#define VER_FRM         23
//#define VER_FRM         2331

#define VER_FRM         2338

#define FRM_ALGO_v2     1
//#define BUFFERING       1
#define FILTERING       1
//#define N_gF            128
#define ARU_NEW         1
//#define ARU_INV         1
//#define NEW_COEF        1
//#define NEW_CHK         1
//#define ARU_CONST       1
#ifdef ARU_CONST
#define ARU_COEF        2
#endif

#elif FRM_ALGO == 3
#define FRM_ALGO_v3     1
#define VER_FRM         5 // VER_FRM > 1 new fft

#if     VER_FRM == 2
#define NEW_FFT         1
#define FILTERING       1
#define N_FFT           256
#elif   VER_FRM == 3
#define NEW_FFT         1
#define FILTERING       1
#define N_FFT           256
#elif   VER_FRM == 4
#define NEW_FFT         1
#define FILTERING       1
#define N_FFT           256
#elif   VER_FRM == 5
#define NEW_FFT         1
#define FILTERING       1
#define N_FFT           256
#endif

#elif FRM_ALGO == 5
#define VER_FRM         4
#define V5_VER          0
#define FILTERING       0
#define FRM_ALGO_v5     1
#define BUFFERING       1
#define Fs_div_2        1
#define KK              16
#define N_buf           KK*16
#define N_pack          8
#define USE_LONG_FRM    1
#define N_FFT           128
#define N1_FFT          (N_FFT >> 1)
#define N2_FFT          (N_FFT >> 2)
#define NEW_FFT         1
#define N_RMS           128
#endif

#define VER_CAN		5
//v1: v0 + PDO5 (mailbox 28) + PDO6 (mailbox 29); ����� �������� mailbox.
//v2: v1 + version (mailbox 15 - ����, mailbox 30 - �����)
//v3: v2 + SYNC_OFF + JUMP_CHK (mailbox 31)
//v4: v3 + new nodeguard (mailbox 11 - ����, mailbox 23 - �����)
//#define VER_DKP         0
//v1: v0 + ...
//#define VER_RDS         1
//v1: v0 + ����� �������� ��������� ����������� ��������;
//v2: v1 + ��������� ������ ����������� �� ������� �������

// ��������� - 2 ����� = �����
struct Struc_WORD
{
	Uint16 Byte1:8;
	Uint16 Byte2:8;
};

// ����������� - 2 ����� & �����
union Struc_WREG
{
   Uint16 word;
   struct Struc_WORD byte;
};

#define ARRAY(Name, Type)   \
typedef struct {            \
    Type * data;            \
    Uint16 size;            \
    int16  isFull;          \
    int16  isEmpty;         \
} Name;

#define T_SYNC (1)

typedef char int8;
typedef unsigned char Uint8;
//typedef long long int64;
typedef long long Int64;
//typedef unsigned long long Uint64;

#endif //COMMON_H
