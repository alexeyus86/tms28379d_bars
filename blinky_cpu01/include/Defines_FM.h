#ifndef FM_H
#define FM_H

// ��������� ���������� ���������
#define FM_Kod_80   0x01                    // ��� �������� V���=80
#define FM_Kod_70   0x02                    // ��� �������� V���=70
#define FM_Kod_60   0x04                    // ��� �������� V���=60
#define FM_Kod_40   0x08                    // ��� �������� V���=40
#define FM_Kod_00   0x10                    // ��� �������� V���=0
#define FM_Kod_RD   0x20                    // ��� �������� V���=��
#define FM_Kod_NF   0x80                    // ��� �� �� V���
#define FM_ALGORITM 0x40

#define FM_TimeYes   (450)           // ����� ����������� ������ ������� �� ��
#define FM_TimeNo    (900)           // ����� ������ ������� �� ��

#define MAJOR_VER_FM	1
//v0: iir �������
//v1: v0 - iir ������� + ����� �������� ������� ��������� �� �������
//v2: v1 ����� ������ + Can.Pdo1_S.FM.bit.FM_Update = 0, ���� 2 ������ �����, � ������ 1 ������� �� ����������

#if MAJOR_VER_FM
#define NEW_CHK     	0
#endif
#define USE_LONG_FM 	1 // 0 - �������� ��� ����� ����

#define CUTTING_FM  0

#define FM_UPDATE_CONST	0

#define VER_FM          5 // w
//#define VER_FM          4

#if   VER_FM == 1
#define N_CHK		128
#define DS_FM       1
#elif VER_FM == 2
#define N_CHK		128
#define DS_FM       2
#elif VER_FM == 3
#define N_CHK		256
#define DS_FM       1
#elif VER_FM == 4
#define N_CHK		256
#define DS_FM       2
#elif VER_FM == 5
#define N_CHK       512
#define DS_FM       1
#elif VER_FM == 6
#define N_CHK       512
#define DS_FM       2
#elif VER_FM == 7
#define N_CHK       1024
#define DS_FM       1
#elif VER_FM == 8
#define N_CHK       1024
#define DS_FM       2
#endif

#if MAJOR_VER_FM
#define FM_Level_RD 10
#define FM_Level_00 40
#define FM_Level_40 40
#define FM_Level_60 45
#define FM_Level_70 45
#define FM_Level_80 30
#else
#define FM_Level_RD 30
#define FM_Level_00 30
#define FM_Level_40 30
#define FM_Level_60 30
#define FM_Level_70 30
#define FM_Level_80 30
#endif

typedef enum
{
  FM_NEW_LEVEL_SET,  
  FM_OLD_LEVEL_SET  
} FM_LEVEL;

void FM_Init();		// ������������� ��
void FM_Analiz(void);		// ��������� ������ ��
void FM_Reset(void);		// ����� ��

#endif
