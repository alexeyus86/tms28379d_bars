#if NEW_FFT

#include "F28x_Project.h"
#include <stdbool.h>
//#include <stdint.h>

typedef struct {int32 Re; int32 Im;} Complex;

void DFFT (Complex* Y);
void IFFT (Complex* Y);
void Power(int32* E, Complex* Y);
void Mult (Complex* Y, const Complex* B);

#if     N_FFT == 64
#define N_FFT_IT         5
#define N_FFT_SH         12
#include "Const_64.h"
#elif   N_FFT == 128
#define N_FFT_IT         6
#define N_FFT_SH         14
#include "Const_128.h"
#elif   N_FFT == 256
#define N_FFT_IT         7
#define N_FFT_SH         16
#include "Const_256.h"
#elif   N_FFT == 512
#define N_FFT_IT         8
#define N_FFT_SH         18
#include "Const_512.h"
#elif   N_FFT == 1024
#define N_FFT_IT         9
#define N_FFT_SH         20
#include "Const_1024.h"
#elif   N_FFT == 2048
#define N_FFT_IT         10
#define N_FFT_SH         22
#include "Const_2048.h"
#endif //N_FFT

//#define N_FFT_SH      ((N_FFT_IT + 1) << 1)
// use >> N_FFT_SH or use *1/(N_FFT*N_FFT)
// N_it = lg2(N)-1

#ifdef _FLASH
#pragma CODE_SECTION(FFT,  ".TI.ramfunc");
#pragma CODE_SECTION(DFFT, ".TI.ramfunc");
#pragma CODE_SECTION(IFFT, ".TI.ramfunc");
#pragma CODE_SECTION(Mult, ".TI.ramfunc");
#pragma CODE_SECTION(Power,".TI.ramfunc");
#endif //FLASH

static void FFT(Complex* Y, int Dir)
{
    Uint16 i, j, k;
    Complex t;
    Uint16 nLoop = N2_FFT;
    Uint16 StepA = 1;
    Uint16 StepW = N1_FFT;
    Complex* pA;
    Complex* pB;
    const Complex* pW;
    for (i = 0; i < N_FFT_IT; i++)
    {
        pA = Y;
        pB = pA + StepA;
        for (j = 0; j < nLoop; j++)
        {
            pW = W;
            for (k = 0; k < StepA; k++)
            {
                if (Dir)
                {
                    t.Re = ( pB->Re * pW->Re - pB->Im * pW->Im) >> (N_FFT_SH); // t:=A*W
                    t.Im = ( pB->Re * pW->Im + pB->Im * pW->Re) >> (N_FFT_SH);
                    pB->Re = (pA->Re - t.Re) >> 1;                    // B:=A-t
                    pB->Im = (pA->Im - t.Im) >> 1;
                    pA->Re = (pA->Re + t.Re) >> 1;                    // A:=A+t
                    pA->Im = (pA->Im + t.Im) >> 1;
                }
                else
                {
                    t.Re = ( pB->Re * pW->Re + pB->Im * pW->Im) >> (N_FFT_SH); // t:=A*W
                    t.Im = (-pB->Re * pW->Im + pB->Im * pW->Re) >> (N_FFT_SH);
                    pB->Re = (pA->Re - t.Re);                       // B:=A-t
                    pB->Im = (pA->Im - t.Im);
                    pA->Re = (pA->Re + t.Re);                       // A:=A+t
                    pA->Im = (pA->Im + t.Im);
                }
                pW += StepW;
                pA++;
                pB++;
            }
            pA += StepA;
            pB += StepA;
        }
        nLoop = nLoop >> 1;
        StepA = StepA << 1;
        StepW = StepW >> 1;
    }
}

void DFFT(Complex* Y)
{
    Uint16 i;
    Complex t, g, h;
    Complex* pA = Y + 1;
    Complex* pB = Y + N1_FFT - 1;
    const Complex* pW = W + 1;
    FFT(Y, true);
    Y[0].Re = 2 * Y[0].Re;
    Y[0].Im = 0;
    for (i = 1; i < N2_FFT; i++, pA++, pB--, pW++)
    {
        g.Re = pA->Re + pB->Re;
        g.Im = pA->Im - pB->Im;
        h.Re = pA->Im + pB->Im;
        h.Im = pB->Re - pA->Re;
        t.Re = (pW->Re * h.Re - pW->Im * h.Im) >> (N_FFT_SH);
        t.Im = (pW->Re * h.Im + pW->Im * h.Re) >> (N_FFT_SH);
        //t.Re = (pW->Re * h.Re - pW->Im * h.Im) / W[0].Re;
        //t.Im = (pW->Re * h.Im + pW->Im * h.Re) / W[0].Re;
        pA->Re = ( g.Re + t.Re) >> 1;
        pA->Im = ( g.Im + t.Im) >> 1;
        pB->Re = ( g.Re - t.Re) >> 1;
        pB->Im = (-g.Im + t.Im) >> 1;
    }
}

void IFFT(Complex* Y)
{
    Uint16 i;
    Complex g, h, t;
    Complex* pA = Y + 1;
    Complex* pB = Y + N1_FFT - 1;
    const Complex* pW = W + 1;
    for (i = 1; i < N2_FFT; i++, pA++, pB--, pW++)
    {
        g.Re = pA->Re + pB->Re;
        g.Im = pA->Im - pB->Im;
        h.Re = pA->Re - pB->Re;
        h.Im = pA->Im + pB->Im;
        t.Re = (pW->Re * h.Re + pW->Im * h.Im) >> (N_FFT_SH);
        t.Im = (pW->Re * h.Im - pW->Im * h.Re) >> (N_FFT_SH);
        //t.Re = (pW->Re * h.Re + pW->Im * h.Im) / W[0].Re;
        //t.Im = (pW->Re * h.Im - pW->Im * h.Re) / W[0].Re;
        pA->Re = ( g.Re - t.Im) >> 1;
        pA->Im = ( g.Im + t.Re) >> 1;
        pB->Re = ( g.Re + t.Im) >> 1;
        pB->Im = (-g.Im + t.Re) >> 1;
    }
    for (i = 1; i < N1_FFT; i++)
    {
        Uint16 j = Index[i];
        if (j > i)
        {
            t = Y[i];
            Y[i] = Y[j];
            Y[j] = t;
        }
    }
    FFT(Y, false);
}

void Mult(Complex* Y, const Complex* B)
{
     Uint16 i;
     int32 re = 0;
     for (i = 1; i < N2_FFT; i++, Y++, B++)
     {
         re    = (Y->Re * B->Re - Y->Im * B->Im) >> (N_FFT_SH);
         Y->Im = (Y->Re * B->Im + Y->Im * B->Re) >> (N_FFT_SH);
         //re    = (Y->Re * B->Re - Y->Im * B->Im) / W[0].Re;
         //Y->Im = (Y->Re * B->Im + Y->Im * B->Re) / W[0].Re;
         Y->Re = re;
     }
 }

void Power(int32* E, Complex* Y)
{
    Uint16 i;
    for (i = 0; i < N1_FFT; i++, Y++)
    {
//        E[i] = ((Y->Re) * (Y->Re) + (Y->Im) * (Y->Im)) >> 15;
        E[i] = ((Y->Re) * (Y->Re) + (Y->Im) * (Y->Im)) >> (N_FFT_SH + 1);
    }
}

void Conj(Complex* In)
{
    Uint16 i;
    for (i = 1; i < N2_FFT; i++, In++)
    {
        In->Im =-In->Im;
    }
}

void Conv(int32* Out, Complex* In, const Complex* Ref)
{
    Uint16 i;
    for (i = 1; i < N2_FFT; i++, In++, Ref++)
    {

    }
}

#endif //NEW_FFT

//typedef unsigned char   Uint8;
//typedef int             int16;
//typedef long            int32;
//typedef unsigned int    Uint16;
//typedef unsigned long   Uint32;
//typedef float           float32;
//typedef long double     float64;
