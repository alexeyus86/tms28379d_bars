#ifndef VERSION_H
#define VERSION_H

#include "Defines_Common.h"
#include "Defines_CAN.h"
#include "Defines_FM.h"
//#include "Defines_RDS.h"
//#include "Defines_DKP.h"

Uint8 ver_calc(Uint8 a, Uint8 b)
{
    return (a < 25 && b < 10) ? (a * 10 + b) : 255;
}

// �����������!!!
// ver_major__ <10
// ver_minor__ <25
// ������ ����. ��������� ������ - (24.9).(24.9).(24.9).(24.9)
//
//Uint8 ver_major_byte1 = VER_FRM;
//Uint8 ver_minor_byte1 = FRM_ALGO;
//
//// ver fm 2.4.9
//// 2 - ���. ���. 4 - ���. ���������. 9 - ������. �������.
//#ifdef VER_FM_NEW_ALGO
//Uint8 ver_major_byte2 = VER_FM + 10 * VER_FM_NEW_ALGO;
//#else
//Uint8 ver_major_byte2 = VER_FM;
//#endif
//Uint8 ver_minor_byte2 = VER_CAN;
//
//Uint8 ver_major_byte3 = VER_DKP;
//Uint8 ver_minor_byte3 = VER_RDS;

//Uint8 byte_send_1 = ver_calc(ver_minor_byte1, ver_major_byte1);
//Uint8 byte_send_2 = ver_calc(ver_minor_byte2, ver_major_byte2);
//Uint8 byte_send_3 = ver_calc(ver_minor_byte3, ver_major_byte3);
//Uint8 byte_send_4  = 00000000;

#if (VER_FRM > 10)
#define BYTE_SEND_1 VER_FRM
#else
#define BYTE_SEND_1 10 * FRM_ALGO + VER_FRM
#endif
#define BYTE_SEND_2 100 * MAJOR_VER_FM + 10 * VER_FM + FM_UPDATE_CONST
//#define BYTE_SEND_3 100 * VER_DKP + 10 * VER_RDS
#define BYTE_SEND_3 0
#define BYTE_SEND_4 VER_CAN

Uint8 byte_send_1 = (BYTE_SEND_1 < 255) ? BYTE_SEND_1 : 255;
Uint8 byte_send_2 = (BYTE_SEND_2 < 255) ? BYTE_SEND_2 : 255;
Uint8 byte_send_3 = (BYTE_SEND_3 < 255) ? BYTE_SEND_3 : 255;
Uint8 byte_send_4 = (BYTE_SEND_4 < 255) ? BYTE_SEND_4 : 255;

#endif
