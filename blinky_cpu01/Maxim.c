#include "F28x_Project.h" // ������������� �����������
#include "Defines_Common.h" // ����� ����������

#if MAXIM

#define DEBUG_MAXIM         0
#define COEF_MAXIM          0
#define USE_CpuTimer0       0
#define EXTERNAL_GENERATOR  1

inline A0(int value) {GPIO_WritePin(68, value);}
inline A1(int value) {GPIO_WritePin(69, value);}
inline A2(int value) {GPIO_WritePin(70, value);}
inline A3(int value) {GPIO_WritePin(71, value);}
inline D0(int value) {GPIO_WritePin(72, value);}
inline D1(int value) {GPIO_WritePin(73, value);}
inline WR(int value) {GPIO_WritePin(74, value);}

#define A0_INIT() GPIO_SetupPinOptions(68, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(68, GPIO_MUX_CPU1, 0)
#define A1_INIT() GPIO_SetupPinOptions(69, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(69, GPIO_MUX_CPU1, 0)
#define A2_INIT() GPIO_SetupPinOptions(70, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(70, GPIO_MUX_CPU1, 0)
#define A3_INIT() GPIO_SetupPinOptions(71, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(71, GPIO_MUX_CPU1, 0)
#define D0_INIT() GPIO_SetupPinOptions(72, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(72, GPIO_MUX_CPU1, 0)
#define D1_INIT() GPIO_SetupPinOptions(73, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(73, GPIO_MUX_CPU1, 0)
#define WR_INIT() GPIO_SetupPinOptions(74, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(74, GPIO_MUX_CPU1, 0)

#if EXTERNAL_GENERATOR
inline EXT_GEN_DATA     (int value) {GPIO_WritePin(75, value);}
inline EXT_GEN_CLK      (int value) {GPIO_WritePin(76, value);}
inline EXT_GEN_ENABLE   (int value) {GPIO_WritePin(77, value);}

#define EXT_GEN_DATA_INIT()    GPIO_SetupPinOptions(75, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(75, GPIO_MUX_CPU1, 0)
#define EXT_GEN_CLK_INIT()     GPIO_SetupPinOptions(76, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(76, GPIO_MUX_CPU1, 0)
#define EXT_GEN_ENABLE_INIT()  GPIO_SetupPinOptions(77, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(77, GPIO_MUX_CPU1, 0)
#endif //EXTERNAL_GENERATOR

#define TIME_DELAY1 1000L
#define TIME_DELAY2 10000L

#ifdef _FLASH
#pragma CODE_SECTION(Maxim_Init, ".TI.ramfunc");
#pragma CODE_SECTION(Maxim, ".TI.ramfunc");
#pragma CODE_SECTION(Maxim_program, ".TI.ramfunc");
#pragma CODE_SECTION(delay, ".TI.ramfunc");
#endif

void delay(long n)
{
    long count1 = 0;
    while (++count1 < n);
}

void Maxim_program(const bool* Q_coef_A, const bool* Q_coef_B);

/*
bool M_coef_A[2] = {1, 0};
bool M_coef_B[2] = {1, 0};

bool F_coef_A[6] = {1, 0, 1, 0, 1, 0};
bool F_coef_B[6] = {0, 1, 0, 0, 1, 1};

bool Q_coef_A[7] = {1, 1, 0, 0, 1, 1, 1};
bool Q_coef_B[7] = {1, 1, 0, 0, 1, 1, 1};
*/

bool M_coef_A[2] = {0, 0};
bool M_coef_B[2] = {0, 0};

bool F_coef_A[6] = {0, 0, 1, 1, 0, 0}; // 12
bool F_coef_B[6] = {0, 0, 1, 1, 0, 0}; // 12

//bool Q_coef_A[7] = {0, 0, 0, 0, 0, 0, 1}; // 1.00
//bool Q_coef_B[7] = {0, 0, 0, 0, 0, 0, 1}; // 1.00

//bool Q_coef_A[7] = {0, 0, 0, 0, 0, 1, 1}; // 2.00
//bool Q_coef_B[7] = {0, 0, 0, 0, 0, 1, 1}; // 2.00

//bool Q_coef_A[7] = {0, 0, 0, 0, 1, 1, 1}; // 4.00
//bool Q_coef_B[7] = {0, 0, 0, 0, 1, 1, 1}; // 4.00

bool Q_coef_A[7] = {1, 1, 0, 0, 1, 1, 1}; // 4.92
bool Q_coef_B[7] = {1, 1, 0, 0, 1, 1, 1}; // 4.92

//bool Q_coef_A[7] = {1, 1, 1, 0, 1, 1, 1}; // 7.11
//bool Q_coef_B[7] = {1, 1, 1, 0, 1, 1, 1}; // 7.11

//bool Q_coef_A[7] = {0, 0, 0, 1, 1, 1, 1}; // 8.00
//bool Q_coef_B[7] = {0, 0, 0, 1, 1, 1, 1}; // 8.00

//bool Q_coef_A[7] = {0, 0, 1, 1, 1, 1, 1}; // 16.00
//bool Q_coef_B[7] = {0, 0, 1, 1, 1, 1, 1}; // 16.00

//bool Q_coef_A[7] = {0, 1, 1, 1, 1, 1, 1};
//bool Q_coef_B[7] = {0, 1, 1, 1, 1, 1, 1};

char coef_A[8] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
char coef_B[8] = {0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

long maxim_write_delay = 10000;
//long maxim_write_delay = 10;
int maxim_ready = 0;
int maxim_clean_up = 1;

#if EXTERNAL_GENERATOR
int gen_ready = 0;
int gen_clean_up = 1;
bool SDI[16] = {1, 0, 0, 0, // OCT=8
//                1, 1, 1, 0, 1, 0, 0, 0, //232
//                1, 0, 1, 0, 0, 0, 1, 0, 1, 1, //651
//                1, 0, 1, 1, 0, 0, 0, 1, 0, 1, // 709
//                1, 0, 1, 1, 0, 0, 0, 1, 0, 0, // 708
                1, 0, 1, 1, 0, 0, 0, 0, 1, 1, // 707
                0, 0}; // CNF
#endif //EXTERNAL_GENERATOR

void Maxim_Init(void)
{
    EALLOW;

    A0_INIT();
    A1_INIT();
    A2_INIT();
    A3_INIT();
    D0_INIT();
    D1_INIT();
    WR_INIT();

#if EXTERNAL_GENERATOR
    EXT_GEN_DATA_INIT();
    EXT_GEN_CLK_INIT();
    EXT_GEN_ENABLE_INIT();
#endif //EXTERNAL_GENERATOR

    EDIS;

    A0(0);
    D0(0);
    WR(0);

#if EXTERNAL_GENERATOR
    EXT_GEN_DATA(0);
    EXT_GEN_CLK(0);
    EXT_GEN_ENABLE(1);
#endif //EXTERNAL_GENERATOR

#if USE_CpuTimer0
    EALLOW;
    PieVectTable.TINT0 = &TINT0_ISR;
    EDIS;

    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 150, 1.219);
    StartCpuTimer0();
#endif //USE_CpuTimer0
}

void Maxim(void)
{
#if EXTERNAL_GENERATOR

    EXT_GEN_CLK(0);
    EXT_GEN_ENABLE(0);

    static int i_gen = 0;
    static long long count_gen = 0;

    if (++count_gen > maxim_write_delay && !gen_ready)
    {
        if (i_gen < 16)
        {
            delay(TIME_DELAY1);
            EXT_GEN_DATA(SDI[i_gen++]);
            delay(TIME_DELAY1);
            EXT_GEN_CLK(1);
        }
        else
        {
            i_gen = 0;
            maxim_ready = 0;
            delay(TIME_DELAY2);
            EXT_GEN_ENABLE(1);
            gen_ready = 1;
        }
        count_gen = 0;
    }
#endif //EXTERNAL_GENERATOR

    if (gen_ready)
    {
        Maxim_program(Q_coef_A, Q_coef_B);
    }
}

#if USE_CpuTimer0
interrupt void TINT0_ISR(void)      // CPU-Timer 0
{
    GpioDataRegs.GPATOGGLE.bit.GPIOA13 = 1;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
#endif //USE_CpuTimer0

void Maxim_program(const bool* Q_coef_A, const bool* Q_coef_B)
{
    static bool M_coef_A[2] = {0, 0};
    static bool M_coef_B[2] = {0, 0};

    static bool F_coef_A[6] = {0, 0, 1, 1, 0, 0}; // 12
    static bool F_coef_B[6] = {0, 0, 1, 1, 0, 0}; // 12

    char coef_A[8] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
    char coef_B[8] = {0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

    coef_A[0] |= (M_coef_A[0] << 4);
    coef_A[0] |= (M_coef_A[1] << 5);

    coef_A[1] |= (F_coef_A[0] << 4);
    coef_A[1] |= (F_coef_A[1] << 5);
    coef_A[2] |= (F_coef_A[2] << 4);
    coef_A[2] |= (F_coef_A[3] << 5);
    coef_A[3] |= (F_coef_A[4] << 4);
    coef_A[3] |= (F_coef_A[5] << 5);

    coef_A[4] |= (Q_coef_A[0] << 4);
    coef_A[4] |= (Q_coef_A[1] << 5);
    coef_A[5] |= (Q_coef_A[2] << 4);
    coef_A[5] |= (Q_coef_A[3] << 5);
    coef_A[6] |= (Q_coef_A[4] << 4);
    coef_A[6] |= (Q_coef_A[5] << 5);
    coef_A[7] |= (Q_coef_A[6] << 4);
//    coef_A[7] |= (0 << 5);

    coef_B[0] |= (M_coef_B[0] << 4);
    coef_B[0] |= (M_coef_B[1] << 5);

    coef_B[1] |= (F_coef_B[0] << 4);
    coef_B[1] |= (F_coef_B[1] << 5);
    coef_B[2] |= (F_coef_B[2] << 4);
    coef_B[2] |= (F_coef_B[3] << 5);
    coef_B[3] |= (F_coef_B[4] << 4);
    coef_B[3] |= (F_coef_B[5] << 5);

    coef_B[4] |= (Q_coef_B[0] << 4);
    coef_B[4] |= (Q_coef_B[1] << 5);
    coef_B[5] |= (Q_coef_B[2] << 4);
    coef_B[5] |= (Q_coef_B[3] << 5);
    coef_B[6] |= (Q_coef_B[4] << 4);
    coef_B[6] |= (Q_coef_B[5] << 5);
    coef_B[7] |= (Q_coef_B[6] << 4);
//    coef_B[7] |= (0 << 5);

    static int i_max = 0;
    static long long count_maxim = 0;

    WR(1);

    if (++count_maxim > maxim_write_delay)
    {
        if (maxim_clean_up)
        {
            if (i_max < 16)
            {
                A0(0);
                A1(0);
                A2(0);
                A3(0);
                delay(TIME_DELAY1);
                WR(0);
                delay(TIME_DELAY1);
                D0(0);
                D1(0);
                delay(TIME_DELAY1);
                i_max++;
            }
            else
            {
                maxim_clean_up = 0;
                delay(TIME_DELAY2);
                WR(1);
                i_max = 0;
            }
        }
        else
        {
            if (i_max < 16)
            {
                if (i_max < 8)
                {
                    A0((coef_A[i_max] & (1 << 0)) >> 0);
                    A1((coef_A[i_max] & (1 << 1)) >> 1);
                    A2((coef_A[i_max] & (1 << 2)) >> 2);
                    A3((coef_A[i_max] & (1 << 3)) >> 3);
                }
                else
                {
                    A0((coef_B[i_max - 8] & (1 << 0)) >> 0);
                    A1((coef_B[i_max - 8] & (1 << 1)) >> 1);
                    A2((coef_B[i_max - 8] & (1 << 2)) >> 2);
                    A3((coef_B[i_max - 8] & (1 << 3)) >> 3);
                }
                delay(TIME_DELAY1);
                WR(0);
                delay(TIME_DELAY1);
                if (i_max < 8)
                {
                    D0((coef_A[i_max] & (1 << 4)) >> 4);
                    D1((coef_A[i_max] & (1 << 5)) >> 5);
                }
                else
                {
                    D0((coef_B[i_max - 8] & (1 << 4)) >> 4);
                    D1((coef_B[i_max - 8] & (1 << 5)) >> 5);
                }
                delay(TIME_DELAY1);
                i_max++;
            }
            else
            {
                i_max = 0;
                maxim_ready = 1;
            }
        }
        count_maxim = 0;
    }
//    if (++count_maxim > maxim_write_delay)
//    {
//        if (i_max < 16 & maxim_clean_up)
//        {
//            A0(0);
//            A1(0);
//            A2(0);
//            A3(0);
//            delay(TIME_DELAY1);
//            WR(0);
//            delay(TIME_DELAY1);
//            D0(0);
//            D1(0);
//            delay(TIME_DELAY1);
//            maxim_ready = 1;
//            i_max++;
//        }
//        else
//        {
//            if (maxim_clean_up)
//            {
//                maxim_clean_up = 0;
//                delay(TIME_DELAY2);
//                WR(1);
//                maxim_ready = 0;
//                i_max = 0;
//            }
//        }
//        if (i_max < 16 & !maxim_ready)
//        {
//            if (i_max < 8)
//            {
//                A0((coef_A[i_max] & (1 << 0)) >> 0);
//                A1((coef_A[i_max] & (1 << 1)) >> 1);
//                A2((coef_A[i_max] & (1 << 2)) >> 2);
//                A3((coef_A[i_max] & (1 << 3)) >> 3);
//            }
//            else
//            {
//                A0((coef_B[i_max - 8] & (1 << 0)) >> 0);
//                A1((coef_B[i_max - 8] & (1 << 1)) >> 1);
//                A2((coef_B[i_max - 8] & (1 << 2)) >> 2);
//                A3((coef_B[i_max - 8] & (1 << 3)) >> 3);
//            }
//            delay(TIME_DELAY1);
//            WR(0);
//            delay(TIME_DELAY1);
//            if (i_max < 8)
//            {
//                D0((coef_A[i_max] & (1 << 4)) >> 4);
//                D1((coef_A[i_max] & (1 << 5)) >> 5);
//            }
//            else
//            {
//                D0((coef_B[i_max - 8] & (1 << 4)) >> 4);
//                D1((coef_B[i_max - 8] & (1 << 5)) >> 5);
//            }
//            delay(TIME_DELAY1);
//            i_max++;
//        }
//        else
//        {
//            if (!maxim_ready)
//            {
//                i_max = 0;
//                maxim_ready = 1;
//            }
//        }
//        count_maxim = 0;
//    }
}

#endif //MAXIM
