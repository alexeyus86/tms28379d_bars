################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../F2837xD_Headers_nonBIOS_cpu1.cmd 

ASM_SRCS += \
../F2837xD_CodeStartBranch.asm \
../F2837xD_usDelay.asm \
../fir16.asm 

C_SRCS += \
../Channel_FM_v1.c \
../Channel_FRM_v23_3_8.c \
../F2837xD_Adc.c \
../F2837xD_CpuTimers.c \
../F2837xD_DefaultISR.c \
../F2837xD_GlobalVariableDefs.c \
../F2837xD_Gpio.c \
../F2837xD_Ipc.c \
../F2837xD_Ipc_Driver_Util.c \
../F2837xD_PieCtrl.c \
../F2837xD_PieVect.c \
../F2837xD_SysCtrl.c \
../Maxim.c \
../blinky_cpu01.c 

C_DEPS += \
./Channel_FM_v1.d \
./Channel_FRM_v23_3_8.d \
./F2837xD_Adc.d \
./F2837xD_CpuTimers.d \
./F2837xD_DefaultISR.d \
./F2837xD_GlobalVariableDefs.d \
./F2837xD_Gpio.d \
./F2837xD_Ipc.d \
./F2837xD_Ipc_Driver_Util.d \
./F2837xD_PieCtrl.d \
./F2837xD_PieVect.d \
./F2837xD_SysCtrl.d \
./Maxim.d \
./blinky_cpu01.d 

OBJS += \
./Channel_FM_v1.obj \
./Channel_FRM_v23_3_8.obj \
./F2837xD_Adc.obj \
./F2837xD_CodeStartBranch.obj \
./F2837xD_CpuTimers.obj \
./F2837xD_DefaultISR.obj \
./F2837xD_GlobalVariableDefs.obj \
./F2837xD_Gpio.obj \
./F2837xD_Ipc.obj \
./F2837xD_Ipc_Driver_Util.obj \
./F2837xD_PieCtrl.obj \
./F2837xD_PieVect.obj \
./F2837xD_SysCtrl.obj \
./F2837xD_usDelay.obj \
./Maxim.obj \
./blinky_cpu01.obj \
./fir16.obj 

ASM_DEPS += \
./F2837xD_CodeStartBranch.d \
./F2837xD_usDelay.d \
./fir16.d 

OBJS__QUOTED += \
"Channel_FM_v1.obj" \
"Channel_FRM_v23_3_8.obj" \
"F2837xD_Adc.obj" \
"F2837xD_CodeStartBranch.obj" \
"F2837xD_CpuTimers.obj" \
"F2837xD_DefaultISR.obj" \
"F2837xD_GlobalVariableDefs.obj" \
"F2837xD_Gpio.obj" \
"F2837xD_Ipc.obj" \
"F2837xD_Ipc_Driver_Util.obj" \
"F2837xD_PieCtrl.obj" \
"F2837xD_PieVect.obj" \
"F2837xD_SysCtrl.obj" \
"F2837xD_usDelay.obj" \
"Maxim.obj" \
"blinky_cpu01.obj" \
"fir16.obj" 

C_DEPS__QUOTED += \
"Channel_FM_v1.d" \
"Channel_FRM_v23_3_8.d" \
"F2837xD_Adc.d" \
"F2837xD_CpuTimers.d" \
"F2837xD_DefaultISR.d" \
"F2837xD_GlobalVariableDefs.d" \
"F2837xD_Gpio.d" \
"F2837xD_Ipc.d" \
"F2837xD_Ipc_Driver_Util.d" \
"F2837xD_PieCtrl.d" \
"F2837xD_PieVect.d" \
"F2837xD_SysCtrl.d" \
"Maxim.d" \
"blinky_cpu01.d" 

ASM_DEPS__QUOTED += \
"F2837xD_CodeStartBranch.d" \
"F2837xD_usDelay.d" \
"fir16.d" 

C_SRCS__QUOTED += \
"../Channel_FM_v1.c" \
"../Channel_FRM_v23_3_8.c" \
"../F2837xD_Adc.c" \
"../F2837xD_CpuTimers.c" \
"../F2837xD_DefaultISR.c" \
"../F2837xD_GlobalVariableDefs.c" \
"../F2837xD_Gpio.c" \
"../F2837xD_Ipc.c" \
"../F2837xD_Ipc_Driver_Util.c" \
"../F2837xD_PieCtrl.c" \
"../F2837xD_PieVect.c" \
"../F2837xD_SysCtrl.c" \
"../Maxim.c" \
"../blinky_cpu01.c" 

ASM_SRCS__QUOTED += \
"../F2837xD_CodeStartBranch.asm" \
"../F2837xD_usDelay.asm" \
"../fir16.asm" 


