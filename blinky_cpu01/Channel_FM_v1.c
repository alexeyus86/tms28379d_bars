#include "Defines_Common.h"                 // ����� ����������
//#include "DSP281x_Device.h"                 // ������������� �����������
#include "Defines_CAN.h"                    // ��������� ������ ��� CAN
#include "Defines_FM.h"                     // ��������� ������ ��� ��
#include <math.h>
//#if MAJOR_VER_FM //NEW_CHK

#if N_CHK == 512
    #define CALC_FM_RETURN_FACTOR 4
#elif N_CHK == 256
    #define CALC_FM_RETURN_FACTOR 8
#elif N_CHK == 128
    #define CALC_FM_RETURN_FACTOR 16
#else
    #define CALC_FM_RETURN_FACTOR 0
    #error "N_CHK ����� �������"
#endif //FM_InputDataBuff.size

int16 is_FM_InputDataBuff = 0;
int16 FM_InputDataBuff[N_CHK];
#ifdef _FLASH
#pragma CODE_SECTION(FM_InputDataBuff, "CM_Buffer");
#endif //FLASH
Uint16 th_fm = 255;
Uint16 th_fm2 = 32;
Uint16 fm_cut = 100;

extern struct CAN Can;

static int16 factors[10][2] = { { 1, 0 }, { 2, 50 }, { 3, 83 }, { 4, 108 },
                              { 8, 159 }, { 10, 171 }, { 15, 192 }, { 20, 203 },
                              { 20, 203 }, { 20, 203 } };

inline Uint8 FM_Protocol(Uint16 value)
{
    int16 i = value / 100;
    return value > 1024 ? 0xFF : factors[i][1] + value / factors[i][0];
}


#if USE_LONG_FM
#if DS_FM == 1
static const long Fs_FM = 2250;
static const long coef1_80 = 1002;
static const long coef2_80 = 213;
static const long coef1_70 = 963;
static const long coef2_70 = 351;
static const long coef1_60 = 905;
static const long coef2_60 = 481;
static const long coef1_40 = 829;
static const long coef2_40 = 602;
static const long coef1_00 = 737;
static const long coef2_00 = 712;
static const long coef1_RD = 631;
static const long coef2_RD = 807;

#elif DS_FM == 2
static const float Fs_FM = 1125;
static const long coef1_80 = 935;
static const long coef2_80 = 416;
static const long coef1_70 = 784;
static const long coef2_70 = 658;
static const long coef1_60 = 572;
static const long coef2_60 = 848;
static const long coef1_40 = 316;
static const long coef2_40 = 973;
static const long coef1_00 = 35;
static const long coef2_00 = 1023;
static const long coef1_RD = -247;
static const long coef2_RD = 993;
#elif DS_FM == 4
static const float Fs_FM = 562.5;
static const long coef1_80 = 685;
static const long coef2_80 = 760;
static const long coef1_70 = 177;
static const long coef2_70 = 1008;
static const long coef1_60 = -383;
static const long coef2_60 = 949;
static const long coef1_40 = -828;
static const long coef2_40 = 601;
static const long coef1_00 = -1021;
static const long coef2_00 = 71;
static const long coef1_RD = -904;
static const long coef2_RD = -480;
#endif //DS_FM
#else
#if DS_FM == 1
static const float Fs_FM = 2250;
static const float coef1_00 = 0.719339;
static const float coef2_00 = 0.694660;
static const float coef1_40 = 0.809016;
static const float coef2_40 = 0.587786;
static const float coef1_60 = 0.882947;
static const float coef2_60 = 0.469473;
static const float coef1_70 = 0.939692;
static const float coef2_70 = 0.342021;
static const float coef1_80 = 0.978148;
static const float coef2_80 = 0.207912;
static const float coef1_RD = 0.615660;
static const float coef2_RD = 0.788012;
#elif DS_FM == 2
static const float Fs_FM = 1125;
static const float coef1_00 = 0.0348959;
static const float coef2_00 = 0.999391;
static const float coef1_40 = 0.309014;
static const float coef2_40 = 0.951057;
static const float coef1_60 = 0.559191;
static const float coef2_60 = 0.829039;
static const float coef1_70 = 0.766043;
static const float coef2_70 = 0.642789;
static const float coef1_80 = 0.913545;
static const float coef2_80 = 0.406738;
static const float coef1_RD =-0.241926;
static const float coef2_RD = 0.970295;
#elif DS_FM == 4
static const float Fs_FM = 562.5;
static const float coef1_80 = 0.669129;
static const float coef2_80 = 0.743146;
static const float coef1_70 = 0.173645;
static const float coef2_70 = 0.984808;
static const float coef1_60 = -0.374611;
static const float coef2_60 = 0.927182;
static const float coef1_40 = -0.80902;
static const float coef2_40 = 0.587781;
static const float coef1_00 = -0.997565;
static const float coef2_00 = 0.0697493;
static const float coef1_RD = -0.882944;
static const float coef2_RD = -0.469479;
#endif //DS_FM
#endif //USE_LONG_FM

#if USE_LONG_FM
static Uint16 calc_FM(int16 *, int16, const int32, const int32, const float);
#else
static Uint16 calc_FM(int16 *, int16, const float, const float, const float);
#endif //USE_LONG_FM

//#define bitness 5
//#define size    (1 << bitness)

#ifdef _FLASH                       // Flash-����������
#pragma CODE_SECTION(calc_FM,   ".TI.ramfunc");
#pragma CODE_SECTION(FM_Init,   ".TI.ramfunc");
#pragma CODE_SECTION(FM_Reset,  ".TI.ramfunc");
#pragma CODE_SECTION(FM_Update, ".TI.ramfunc");
#endif //FLASH

Uint16 FM_InputData;                // ������� ������ ��� �������� ��
Uint8  FM_DataReady;

Uint16 Kod_FM2;                     // �������� ���
static Uint16 Timer_FM2;            // ����� �������������
static Uint16 Zero_FM2;             // ����� �� ������ �������

Uint16 Amplituda80;                 // ��������� ������� �� ������ ������� V���=80 �� ������ (DS_FM*N_gf*EvbRegs.T3PR/15e7) = 28ms
Uint16 Amplituda70;                 // ��������� ������� �� ������ ������� V���=70 �� ������ 56��
Uint16 Amplituda60;                 // ��������� ������� �� ������ ������� V���=60 �� ������ 56��
Uint16 Amplituda40;                 // ��������� ������� �� ������ ������� V���=84 �� ������ 56��
Uint16 Amplituda00;                 // ��������� ������� �� ������ ������� V���=0  �� ������ 56��
Uint16 AmplitudaRD;                 // ��������� ������� �� ������ ������� V���=�� �� ������ 56��

static Uint16 Kod_with_ManyFreq;    // ���� V��� ������� ������� ��������� � ���� �������������
Uint16 Visual_Kod_FM;               // ���� V��� ������������ �� ������������ ���������

//extern Uint32 counter;

//������������� ��
void FM_Init()       // ������������� ��
{
    FM_Reset();                     // ����� ��������
}

//��������� ���������� ������ ��
void FM_Reset()                     // ����� �������� ��
{
    FM_InputData = 0;               // ��������� ������� ������ ��� �������� ��
    FM_DataReady = 0;

    Kod_FM2 = 0;                    // ��������� �������� ��� V���
    Timer_FM2 = 0;                  // ��������� ����� �������������
    Zero_FM2 = 0;                   // ��������� ����� �� ������ �������
    Visual_Kod_FM = 0;              // ��������� ���� V��� ������������ �� ������������ ���������

    Amplituda80 = 0;                // ��������� �������� ������� �� ������ ������� V���=80
    Amplituda70 = 0;                // ��������� �������� ������� �� ������ ������� V���=70
    Amplituda60 = 0;                // ��������� �������� ������� �� ������ ������� V���=60
    Amplituda40 = 0;                // ��������� �������� ������� �� ������ ������� V���=40
    Amplituda00 = 0;                // ��������� �������� ������� �� ������ ������� V���=0
    AmplitudaRD = 0;                // ��������� �������� ������� �� ������ ������� V���=��

    Kod_with_ManyFreq = 0;          // ��������� ���� V��� ������� ������� ��������� � ���� �������������
}

/**
 *  @brief ��������� ������ ��
 *
 *  @par ��������
 *  - ������������� ��������� �������� ����� ���������
 *  - ���������� �������� �������
 *  - ���������� ���������� ���������
 *  - ������������ �����-� �������� � ������� 12��
 *  - ����������� ����. ��������� �� 12��
 *  - ����������� �������� ������� � ������
 *  - �������� ������� ������ ������ � ������
 *  - ��������� ���������� ����������� ������ �������� �������
 *  - ��������� ���������� ������ ������� �������� �������
 *  - ��������� ���������� ����������� ������ ���������� ������
 *  - ��������� ���������� ������ ������� ���������� ������
 *  - ���������� ������ Pdo1
 *
 *  @param ���
 *  @return ���
*/

void FM_Update(Uint16 Input_FM)    // ��������� ������ ��
{
    static int16 ind_calc = 0;
    if (is_FM_InputDataBuff)
    {
#if CUTTING_FM
    	Uint16 max_ampl = 0;
#endif //CUTTING_FM
		static Uint16 Amplituda80_t = 0;
        static Uint16 Amplituda70_t = 0;
        static Uint16 Amplituda60_t = 0;
        static Uint16 Amplituda40_t = 0;
        static Uint16 Amplituda00_t = 0;
        static Uint16 AmplitudaRD_t = 0;

        switch (ind_calc)
        {
        case 1: Amplituda80_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_80, coef2_80, Fs_FM); /*if (Amplituda80 > th_fm) Amplituda80 = th_fm;*/ if (Amplituda80 < th_fm2) Amplituda80 = 0; Can.Pdo1_S.FM.bit.FM_Update = 0; break;
        case 2: Amplituda70_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_70, coef2_70, Fs_FM); /*if (Amplituda70 > th_fm) Amplituda70 = th_fm;*/ if (Amplituda70 < th_fm2) Amplituda70 = 0; Can.Pdo1_S.FM.bit.FM_Update = 0; break;
        case 3: Amplituda60_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_60, coef2_60, Fs_FM); /*if (Amplituda60 > th_fm) Amplituda60 = th_fm;*/ if (Amplituda60 < th_fm2) Amplituda60 = 0; Can.Pdo1_S.FM.bit.FM_Update = 0; break;
        case 4: Amplituda40_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_40, coef2_40, Fs_FM); /*if (Amplituda40 > th_fm) Amplituda40 = th_fm;*/ if (Amplituda40 < th_fm2) Amplituda40 = 0; Can.Pdo1_S.FM.bit.FM_Update = 0; break;
        case 5: Amplituda00_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_00, coef2_00, Fs_FM); /*if (Amplituda00 > th_fm) Amplituda00 = th_fm;*/ if (Amplituda00 < th_fm2) Amplituda00 = 0; Can.Pdo1_S.FM.bit.FM_Update = 0; break;
#if CUTTING_FM
        case 6: AmplitudaRD_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_RD, coef2_RD, Fs_FM); /*if (AmplitudaRD > th_fm) AmplitudaRD = th_fm;*/ if (AmplitudaRD < th_fm2) AmplitudaRD = 0; Can.Pdo1_S.FM.bit.FM_Update = 0; break;
#else //CUTTING_FM
        case 6: AmplitudaRD_t = calc_FM(FM_InputDataBuff, N_CHK, coef1_RD, coef2_RD, Fs_FM); /*if (AmplitudaRD > th_fm) AmplitudaRD = th_fm;*/ if (AmplitudaRD < th_fm2) AmplitudaRD = 0; Can.Pdo1_S.FM.bit.FM_Update = 0;

        Amplituda80 = Amplituda80_t;
        Amplituda70 = Amplituda70_t;
        Amplituda60 = Amplituda60_t;
        Amplituda40 = Amplituda40_t;
        Amplituda00 = Amplituda00_t;
        AmplitudaRD = AmplitudaRD_t;

        Can.Pdo1_S.FM_Ampl_80 = FM_Protocol(Amplituda80);
        Can.Pdo1_S.FM_Ampl_70 = FM_Protocol(Amplituda70);
        Can.Pdo1_S.FM_Ampl_60 = FM_Protocol(Amplituda60);
        Can.Pdo1_S.FM_Ampl_40 = FM_Protocol(Amplituda40);
        Can.Pdo1_S.FM_Ampl_00 = FM_Protocol(Amplituda00);
        Can.Pdo1_S.FM_Ampl_RD = FM_Protocol(AmplitudaRD);

        ind_calc = 0;
        is_FM_InputDataBuff = 0;
        Can.Pdo1_S.FM.bit.FM_Update = 1;
        break;
#endif //CUTTING_FM
//        case 7: ind_calc = 0; is_FM_InputDataBuff = 0; Can.Pdo1_S.FM.bit.FM_Update = 1; break;
#if CUTTING_FM
        case 7 :
        	Amplituda80 = Amplituda80_t;
        	Amplituda70 = Amplituda70_t;
        	Amplituda60 = Amplituda60_t;
        	Amplituda40 = Amplituda40_t;
        	Amplituda00 = Amplituda00_t;
        	AmplitudaRD = AmplitudaRD_t;

			if (Amplituda80 > max_ampl) max_ampl = Amplituda80;
			if (Amplituda70 > max_ampl) max_ampl = Amplituda70;
			if (Amplituda60 > max_ampl) max_ampl = Amplituda60;
			if (Amplituda40 > max_ampl) max_ampl = Amplituda40;
			if (Amplituda00 > max_ampl) max_ampl = Amplituda00;

			fm_cut = (max_ampl > 50) ? max_ampl>>1 : 32;

			if (Amplituda80 < ((Uint16)(max_ampl/10) + fm_cut)) Amplituda80 = 0;
			if (Amplituda70 < ((Uint16)(max_ampl/10) + fm_cut)) Amplituda70 = 0;
			if (Amplituda60 < ((Uint16)(max_ampl/10) + fm_cut)) Amplituda60 = 0;
			if (Amplituda40 < ((Uint16)(max_ampl/10) + fm_cut)) Amplituda40 = 0;
			if (Amplituda00 < ((Uint16)(max_ampl/10) + fm_cut)) Amplituda00 = 0;

			ind_calc = 0;
			is_FM_InputDataBuff = 0;
			Can.Pdo1_S.FM.bit.FM_Update = 1;
			break;
#endif //CUTTING_FM
        }
        ind_calc++;
    }
    // ��������� �����������
    // ����������� �������� �������(���������� �� ���������) � ������
    Can.Pdo1_S.FM.bit.FM_Update = 1;
    // �������� ������� ������ ������ � ������
    Kod_with_ManyFreq = 00;
    if (Amplituda80 >= FM_Level_80) Kod_with_ManyFreq |= FM_Kod_80;
    if (Amplituda70 >= FM_Level_70) Kod_with_ManyFreq |= FM_Kod_70;
    if (Amplituda60 >= FM_Level_60) Kod_with_ManyFreq |= FM_Kod_60;
    if (Amplituda40 >= FM_Level_40) Kod_with_ManyFreq |= FM_Kod_40;
    if (Amplituda00 >= FM_Level_00) Kod_with_ManyFreq |= FM_Kod_00;
    if (AmplitudaRD >= FM_Level_RD) Kod_with_ManyFreq |= FM_Kod_RD;
    if (!Kod_with_ManyFreq) Kod_with_ManyFreq = FM_Kod_NF;
    if (Kod_FM2 == Kod_with_ManyFreq)// ���������� ����� ���������� ������
    {
        if (Timer_FM2 > FM_TimeYes) // ��������� ���������� ����������� ������ ���������� ������
        {
#if !DEBUG_NODEGUARD
            Visual_Kod_FM = Kod_FM2; // ������ ������ ��� ��������������
#endif //!DEBUG_NODEGUARD
        }
        else Timer_FM2++;
        Zero_FM2 = 0;
    }
    else
    {
        if ((Kod_FM2 == FM_Kod_NF) || (Zero_FM2 > FM_TimeNo)) // ��������� ���������� ������ ������� ���������� ������
        {
            Kod_FM2 = Kod_with_ManyFreq;
            Timer_FM2 = 0;
        }
        else Zero_FM2++;
    }
}
#if USE_LONG_FM
static Uint16 calc_FM(int16 data[], int16 n, const int32 coef1, const int32 coef2, const float Fs_FM)
{
    int16 i    = 0;
    int32 Re   = data[0];
    int32 Im   = 0;
    int32 Re_p = 0;
    int32 Im_p = 0;
    for (i = 0; i < n; ++i)
    {
        Re_p = Re;
        Im_p = Im;
        Re = ((coef1) * (Re_p) >> 10) - ((coef2) * (Im_p) >> 10) + (int32) data[i];
        Im = ((coef1) * (Im_p) >> 10) + ((coef2) * (Re_p) >> 10);
    }
    float Im_r = Im / Fs_FM;
    float Re_r = Re / Fs_FM;
    return (Uint16)(CALC_FM_RETURN_FACTOR * sqrtf((Re_r * Re_r + Im_r * Im_r)));
}
#else
static Uint16 calc_FM(int16 data[], int16 n, const float coef1, const float coef2, const float Fs_FM)
{
    int16 i    = 0;
    float Re   = data[0];
    float Im   = 0;
    float Re_p = 0;
    float Im_p = 0;
    for (i = 0; i < n; ++i)
    {
        Re_p = Re;
        Im_p = Im;
        Re = (coef1 * Re_p) - (coef2 * Im_p) + (float)data[i];
        Im = (coef1 * Im_p) + (coef2 * Re_p);
    }
    float Im_r = Im / Fs_FM;
    float Re_r = Re / Fs_FM;
    return (Uint16)(CALC_FM_RETURN_FACTOR * sqrtf((Re_r * Re_r + Im_r * Im_r)));
}
#endif //USE_LONG_FM
//#endif //NEW_CHK

