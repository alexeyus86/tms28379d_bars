//###########################################################################

//###########################################################################

//
// Included Files
//
#include "F28x_Project.h"
#include "F2837xD_Ipc_drivers.h"
#include "Defines_Common.h"
#include "Defines_FRM.h"
#include "Defines_CAN.h"
#include <math.h>

unsigned long long count_BARS = 0;
unsigned long long count_BARS_p = 0;
interrupt void TIMER0_ISR(void); // CPU-Timer 0
int isBARS = 0;

extern volatile struct FRM ARSF_Struct;
extern Uint16 Visual_Kod_FM;

extern Uint16 Amplituda80;
extern Uint16 Amplituda70;
extern Uint16 Amplituda60;
extern Uint16 Amplituda40;
extern Uint16 Amplituda00;
extern Uint16 AmplitudaRD;

//////////////////////////////
//CHM
#define PWM1_PERIOD 22222
#define PWM1_CMPR25 0

//FRM
#define FC 3348
//#define PWM2_PERIOD 2986-1
//#define PWM2_PERIOD 1492-1
#define PWM2_PERIOD 746-1
//#define PWM2_PERIOD 373-1
#define PWM2_CMPR25 PWM2_PERIOD>>2

volatile int aaaaa = 0;
extern void ARSF_Receive(void);
Uint16 Version = 0;
/*volatile*/ struct CAN Can;
extern Uint16 ARSF_Update(int16);
extern void ARSF_Init();

extern Uint8  FM_DataReady;
extern Uint16 FM_InputData;
extern int16 FM_InputDataBuff[N_CHK];
extern int16 is_FM_InputDataBuff;
extern void FM_Update(Uint16);
extern void FM_Init();

int meanA = 0;
int meanB = 0;

//////////////////////////////

#if MAXIM
    extern void Maxim_Init();
    extern void Maxim();
    extern int gen_ready;
    extern int maxim_ready;
    extern int maxim_clean_up;
#endif

//
// Defines
//
#define RESULTS_BUFFER_SIZE 128
//
// Function Prototypes
//
void ConfigureADC(void);
void ConfigureEPWM1(void);
void ConfigureEPWM2(void);
void SetupADCEpwm1(Uint16 channel);
void SetupADCEpwm2(Uint16 channel);
interrupt void ADCA1_ISR(void);
interrupt void ADCB1_ISR(void);
float calc_FRM(int16*, int16, float);
//
// Globals
//
int16 AdcaResults[RESULTS_BUFFER_SIZE];
int16 AdcaResultsDisp[RESULTS_BUFFER_SIZE];
int16 AdcaResult;
int16 resultsIndexa;
volatile Uint16 bufferaFull;

int16 AdcbResults[RESULTS_BUFFER_SIZE];
int16 AdcbResultsDisp[RESULTS_BUFFER_SIZE];
int16 AdcbResult;
int16 resultsIndexb;
volatile Uint16 bufferbFull;

//
//int32 coef_1 = 0;
//int32 coef_2 = 0;


const float fc = 3348.21;
float K = 5;

float tmpa;
float tmpb;

#define _DEBUG 0

#ifdef _FLASH
    #pragma CODE_SECTION(calc_FRM, ".TI.ramfunc");
#endif

//
// Main
//
void
main(void)
{

//
// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
// This example function is found in the F2837xD_SysCtrl.c file.
//
   InitSysCtrl();

#ifdef _STANDALONE
#ifdef _FLASH
//
// Send boot command to allow the CPU2 application to begin execution
//
IPCBootCPU2(C1C2_BROM_BOOTMODE_BOOT_FROM_FLASH);
#else
//
// Send boot command to allow the CPU2 application to begin execution
//
IPCBootCPU2(C1C2_BROM_BOOTMODE_BOOT_FROM_RAM);
#endif
#endif

//
// Call Flash Initialization to setup flash waitstates
// This function must reside in RAM
//
#ifdef _FLASH
    memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (size_t)&RamfuncsLoadSize);
#endif //FLASH

   InitFlash();

//
// Step 2. Initialize GPIO:
// This example function is found in the F2837xD_Gpio.c file and
// illustrates how to set the GPIO to it's default state.
//
    InitGpio(); // Skipped for this example
    EALLOW;
//
// TODO Add code to allow configuration of GPADIR from CPU02 using IPC
//

// ARU
    GPIO_SetupPinOptions(KUO_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(KUO_GPIO, GPIO_MUX_CPU1, 0); GPIO_WritePin(KUO_GPIO, 0);
    GPIO_SetupPinOptions(KU1_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(KU1_GPIO, GPIO_MUX_CPU1, 0); GPIO_WritePin(KU1_GPIO, 0);

// FM_det
#define FM_det_GPIO 133
    GPIO_SetupPinOptions(FM_det_GPIO, GPIO_INPUT, GPIO_PULLUP); GPIO_SetupPinMux(FM_det_GPIO, GPIO_MUX_CPU1, 0);

//
// TODO rename LEDs
//

// LEDs
#define LED1_GPIO 28
#define LED2_GPIO 29
#define LED3_GPIO 30
#define LED4_GPIO 31
#define LED5_GPIO 32
#define LED6_GPIO 33

// LED1
    GPIO_SetupPinOptions(LED1_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(LED1_GPIO, GPIO_MUX_CPU1, 0);
// LED2
    GPIO_SetupPinOptions(LED2_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(LED2_GPIO, GPIO_MUX_CPU1, 0);
// LED3
    GPIO_SetupPinOptions(LED3_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(LED3_GPIO, GPIO_MUX_CPU1, 0);
// LED4
    GPIO_SetupPinOptions(LED4_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(LED4_GPIO, GPIO_MUX_CPU1, 0);
// LED5
    GPIO_SetupPinOptions(LED5_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(LED5_GPIO, GPIO_MUX_CPU1, 0);
// LED6
    GPIO_SetupPinOptions(LED6_GPIO, GPIO_OUTPUT, GPIO_PUSHPULL); GPIO_SetupPinMux(LED6_GPIO, GPIO_MUX_CPU1, 0);

// BARS / PUAV
#define R1_GPIO 40
    GPIO_SetupPinOptions(R1_GPIO, GPIO_INPUT, GPIO_PULLUP); GPIO_SetupPinMux(R1_GPIO, GPIO_MUX_CPU1, 0);


    EDIS;
//
// Step 3. Clear all interrupts and initialize PIE vector table:
// Disable CPU interrupts
//
    DINT;

//
// Initialize the PIE control registers to their default state.
// The default state is all PIE interrupts disabled and flags
// are cleared.
// This function is found in the F2837xD_PieCtrl.c file.
//
    InitPieCtrl();

//
// Disable CPU interrupts and clear all CPU interrupt flags:
//
    IER = 0x0000;
    IFR = 0x0000;

//
// Initialize the PIE vector table with pointers to the shell Interrupt
// Service Routines (ISR).
// This will populate the entire table, even if the interrupt
// is not used in this example.  This is useful for debug purposes.
// The shell ISR routines are found in F2837xD_DefaultIsr.c.
// This function is found in F2837xD_PieVect.c.
//
    InitPieVectTable();

//
// Map ISR functions
//
    EALLOW;
    PieVectTable.ADCA1_INT = &ADCA1_ISR; //function for ADCA interrupt 1
    PieVectTable.ADCB1_INT = &ADCB1_ISR; //function for ADCA interrupt 1
//    PieVectTable.ADCB1_INT = &TIMER0_ISR; //function for ADCA interrupt 1
    EDIS;

//
// Configure the ADC and power it up
//
    ConfigureADC();

//
// Configure the ePWM
//
    ConfigureEPWM1();
    ConfigureEPWM2();

//
// Setup the ADC for ePWM triggered conversions on channel 0
//
    SetupADCEpwm1(0);
    SetupADCEpwm2(0);

//
// Enable global Interrupts and higher priority real-time debug events:
//
    IER |= M_INT1; //Enable group 1 interrupts
    EINT;  // Enable Global interrupt INTM
    ERTM;  // Enable Global realtime interrupt DBGM

//

// Initialize results buffer
//
    for(resultsIndexb = 0; resultsIndexb < RESULTS_BUFFER_SIZE; resultsIndexb++)
    {
        AdcaResults[resultsIndexb] = 0;
        AdcbResults[resultsIndexb] = 0;
    }
    resultsIndexb = 0;
    bufferaFull = 0;
    bufferbFull = 0;
//
// enable PIE interrupt
//
    PieCtrlRegs.PIEIER1.bit.INTx1 = 1; // ADCA
    PieCtrlRegs.PIEIER1.bit.INTx2 = 1; // ADCB
//
// sync ePWM
//
    EALLOW;
    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV = 1;
    EDIS;

    EALLOW;
    PieVectTable.TIMER0_INT = &TIMER0_ISR;
    EDIS;

    PieCtrlRegs.PIEIER1.bit.INTx7 = 1; // CpuTimer0

    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 200, 16000);
    StartCpuTimer0();
//
//start ePWM
//
    EPwm1Regs.ETSEL.bit.SOCAEN = 1;  //enable SOCA
    EPwm1Regs.TBCTL.bit.CTRMODE = 0; //unfreeze, and enter up count mode

    EPwm2Regs.ETSEL.bit.SOCAEN = 1;  //enable SOCA
    EPwm2Regs.TBCTL.bit.CTRMODE = 0; //unfreeze, and enter up count mode

    ARSF_Init();
    FM_Init();

//
//start MAXIM
//
// TODO separate generator and Maxim filter
#if MAXIM
    Maxim_Init();
    while (!gen_ready || !maxim_ready)
    {
        Maxim();
    }
#endif

    isBARS = GPIO_ReadPin(R1_GPIO);

#if _DEBUG
    while(1)
    {
        GPIO_WritePin(LED1_GPIO, 1);
        GPIO_WritePin(LED2_GPIO, 1);
        GPIO_WritePin(LED3_GPIO, 1);
        GPIO_WritePin(LED4_GPIO, 1);
        GPIO_WritePin(LED5_GPIO, 1);
        GPIO_WritePin(LED6_GPIO, 1);
        GPIO_WritePin(LED84_GPIO, 1);
        DELAY_US(1000 * 500);
        GPIO_WritePin(LED1_GPIO, 0);
        GPIO_WritePin(LED2_GPIO, 0);
        GPIO_WritePin(LED3_GPIO, 0);
        GPIO_WritePin(LED4_GPIO, 0);
        GPIO_WritePin(LED5_GPIO, 0);
        GPIO_WritePin(LED6_GPIO, 0);
        GPIO_WritePin(LED84_GPIO, 0);
        DELAY_US(1000 * 500);
    }
#else
    while (1)
    {
        if (bufferaFull)
        {
            tmpa = calc_FRM(AdcaResults, RESULTS_BUFFER_SIZE, fc*K);
            bufferaFull=0;
            int resultsIndex0 = 0;
            for(resultsIndex0 = 0; resultsIndex0 < RESULTS_BUFFER_SIZE; resultsIndex0++)
            {
                AdcaResultsDisp[resultsIndex0] = AdcaResults[resultsIndex0];
            }
        }
        if (bufferbFull)
        {
            tmpb = calc_FRM(AdcbResults, RESULTS_BUFFER_SIZE, fc*K);
            bufferbFull=0;
            int resultsIndex0 = 0;
            for(resultsIndex0 = 0; resultsIndex0 < RESULTS_BUFFER_SIZE; resultsIndex0++)
            {
                AdcbResultsDisp[resultsIndex0] = AdcbResults[resultsIndex0];
            }
        }

        ARSF_Receive();

        if (FM_DataReady)
        {
            FM_Update(FM_InputData);
            FM_DataReady = 0;
        }
    }
#endif
}

float
calc_FRM(int16 data[], int16 n, float Fs)
{
    int32 coef_1 = (int32)(1024.0 * cosf(2 * 3.1416 * 3348.21 / Fs));
    int32 coef_2 = (int32)(1024.0 * sinf(2 * 3.1416 * 3348.21 / Fs));

    int32 Re   = data[0];
    int32 Im   = 0;
    int32 Re_p = 0;
    int32 Im_p = 0;
    int16 i    = 0;
    for (i = 0; i < n; ++i)
    {
        Re_p = Re;
        Im_p = Im;
        Re = ((coef_1) * (Re_p) >> 10) - ((coef_2) * (Im_p) >> 10) + (int32) data[i];
        Im = ((coef_1) * (Im_p) >> 10) + ((coef_2) * (Re_p) >> 10);
    }
    float Im_r = Im / (Fs);
    float Re_r = Re / (Fs);
//    return (2 * fc * sqrt(Re_r * Re_r + Im_r * Im_r));
    return (Re_r * Re_r + Im_r * Im_r);
}

void
ConfigureADC(void)
{
    EALLOW;

    //
    //write configurations
    //
    AdcaRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4
    AdcbRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4 (200 MHz / 4) = 50 MHz
    AdcSetMode(ADC_ADCA, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCB, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);

    //
    //Set pulse positions to late
    //
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    AdcbRegs.ADCCTL1.bit.INTPULSEPOS = 1;

    //
    //power up the ADCs
    //
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    AdcbRegs.ADCCTL1.bit.ADCPWDNZ = 1;

    //
    //delay for 1ms to allow ADC time to power up
    //
    DELAY_US(1000);

    EDIS;
}

//
// ConfigureEPWM - Configure EPWM SOC and compare values
//
void
ConfigureEPWM1(void)
{
    EALLOW;
    // Assumes ePWM clock is already enabled
    EPwm1Regs.ETSEL.bit.SOCAEN  = 0; // Disable SOC on A group
    EPwm1Regs.ETSEL.bit.SOCASEL = 4; // Select SOC on up-count
    EPwm1Regs.ETPS.bit.SOCAPRD  = 1; // Generate pulse on 1st event
    EPwm1Regs.CMPA.bit.CMPA = PWM1_CMPR25; // Set compare A value
    EPwm1Regs.TBPRD = PWM1_PERIOD; // Set period
    EPwm1Regs.TBCTL.bit.CTRMODE = 3; // freeze counter
    EDIS;
}

void
ConfigureEPWM2(void)
{
    EALLOW;

//    EPwm2Regs.TBCTL.bit.PRDLD = TB_IMMEDIATE;
//    EPwm2Regs.ETSEL.bit.SOCAEN = 0;       // Disable SOC on A group
//    EPwm2Regs.ETSEL.bit.SOCASEL = 4;      // Select SOC on up-count
//    EPwm2Regs.ETPS.bit.SOCAPRD = 1;       // Generate pulse on 1st event
//    EPwm2Regs.TBPRD = PWM2_PERIOD; // Set period
//    EPwm2Regs.TBPHS.bit.TBPHS = 0; // Phase is 0
//    EPwm2Regs.TBCTR = 0; // Clear counter
//    EPwm2Regs.CMPA.bit.CMPA = 0; // Set compare A value
//    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up and down
//    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE; // EPWM1 is the Master
//    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
//    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // Clock ratio to SYSCLKOUT
//    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;
//    EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
//    EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
//    EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // optional
//    EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; // optional
//    EPwm2Regs.AQCTLA.bit.ZRO = AQ_SET;
//    EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//    EPwm2Regs.AQCTLB.bit.ZRO = AQ_SET; // optional
//    EPwm2Regs.AQCTLB.bit.CBU = AQ_CLEAR; // optional
//    EPwm2Regs.HRCNFG.all = 0x0; // clear all bits first
//    EPwm2Regs.HRCNFG.bit.EDGMODE = HR_FEP; // Control Falling Edge Positio
//    EPwm2Regs.HRCNFG.bit.CTLMODE = HR_CMP; // CMPAHR controls the MEP
//    EPwm2Regs.HRCNFG.bit.HRLOAD = HR_CTR_ZERO; // Shadow load on CTR=Zero
//    EPwm2Regs.TBCTL.bit.CTRMODE = 3;      // freeze counter


    // Assumes ePWM clock is already enabled
    EPwm2Regs.ETSEL.bit.SOCAEN  = 0; // Disable SOC on A group
    EPwm2Regs.ETSEL.bit.SOCASEL = 4; // Select SOC on up-count
    EPwm2Regs.ETPS.bit.SOCAPRD  = 1; // Generate pulse on 1st event
    EPwm2Regs.CMPA.bit.CMPA = PWM2_CMPR25; // Set compare A value
    EPwm2Regs.TBPRD = PWM2_PERIOD; // Set period
    EPwm2Regs.TBCTL.bit.CTRMODE = 3; // freeze counter

//    EPwm2Regs.TBCTL.bit.CTRMODE = 3;            // Freeze counter
//    EPwm2Regs.TBCTL.bit.HSPCLKDIV = 0;          // TBCLK pre-scaler = /1
//    EPwm2Regs.TBPRD = PWM2_PERIOD;                   // Set period to 2000 counts (50kHz)
////    EPwm1Regs.CMPA.bit.CMPA = PWM2_PERIOD;
//    EPwm2Regs.ETSEL.bit.SOCAEN  = 0;            // Disable SOC on A group
//    EPwm2Regs.ETSEL.bit.SOCASEL = 2;            // Select SOCA on period match
//    EPwm2Regs.ETSEL.bit.SOCAEN = 1;             // Enable SOCA
//    EPwm2Regs.ETPS.bit.SOCAPRD = 1;             // Generate pulse on 1st event
//    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up and down
//    EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Disable phase loading
//    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // Clock ratio to SYSCLKOUT
//    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

//    EPwm2Regs.ETSEL.bit.SOCAEN  = 0; // Disable SOC on A group
//    EPwm2Regs.ETSEL.bit.SOCASEL = 4; // Select SOC on up-count
//    EPwm2Regs.TBPRD = PWM2_PERIOD; // Set timer period 801 TBCLKs
//    EPwm2Regs.TBPHS.bit.TBPHS = 0; // Phase is 0
//    EPwm2Regs.TBCTR = 0; // Clear counter
//    // // Set Compare values0 //
//    EPwm2Regs.CMPA.bit.CMPA = PWM2_PERIOD; // Set compare A value
//    EPwm2Regs.CMPB.bit.CMPB = PWM2_PERIOD;
//    // // Setup counter mode //
//    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up and down
//    EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Disable phase loading
//
//    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // Clock ratio to SYSCLKOUT
//    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EDIS;
}

//
// SetupADCEpwm - Setup ADC EPWM acquisition window
//
void
SetupADCEpwm1(Uint16 channel)
{
    Uint16 acqps;

    //
    // Determine minimum acquisition window (in SYSCLKS) based on resolution
    //
    if(ADC_RESOLUTION_12BIT == AdcaRegs.ADCCTL2.bit.RESOLUTION)
    {
        acqps = 14; //75ns
    }
    else //resolution is 16-bit
    {
        acqps = 63; //320ns
    }

    //
    //Select the channels to convert and end of conversion flag
    //
    EALLOW;
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = channel;  //SOC0 will convert pin A0
    AdcaRegs.ADCSOC0CTL.bit.ACQPS = PWM1_PERIOD; //sample window is 100 SYSCLK cycles
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C
    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 0; //end of SOC0 will set INT1 flag
    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared
    EDIS;
}

void
SetupADCEpwm2(Uint16 channel)
{
    Uint16 acqps;

    //
    // Determine minimum acquisition window (in SYSCLKS) based on resolution
    //
    if(ADC_RESOLUTION_12BIT == AdcbRegs.ADCCTL2.bit.RESOLUTION)
    {
        acqps = 14; //75ns
    }
    else //resolution is 16-bit
    {
        acqps = 63; //320ns
    }

    //
    //Select the channels to convert and end of conversion flag
    //
    EALLOW;
    AdcbRegs.ADCSOC0CTL.bit.CHSEL = channel;  //SOC0 will convert pin A0
    AdcbRegs.ADCSOC0CTL.bit.ACQPS = PWM2_PERIOD; //sample window is 100 SYSCLK cycles
    AdcbRegs.ADCSOC0CTL.bit.TRIGSEL = 7; //trigger on ePWM2 SOCA/C
//    AdcbRegs.ADCSOC0CTL.bit.TRIGSEL = 1;
    AdcbRegs.ADCINTSEL1N2.bit.INT1SEL = 0; //end of SOC0 will set INT1 flag
    AdcbRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared
    EDIS;
}

//
// ADCA1_ISR - CHM
//
interrupt void
ADCA1_ISR(void)
{
    meanA = AdcaResultRegs.ADCRESULT1;

    AdcaResult = AdcaResultRegs.ADCRESULT0 - 2048;
    AdcaResults[resultsIndexa++] = AdcaResult;

    aaaaa = AdcaResult;

    if (resultsIndexa == RESULTS_BUFFER_SIZE)
    {
//        tmp = calc_FRM(AdcaResults, RESULTS_BUFFER_SIZE, fc*K);
        resultsIndexa = 0;
        bufferaFull = 1;
    }

    static int i = 0;
    if (!is_FM_InputDataBuff)
    {
        if (i < N_CHK)
        {
            FM_InputDataBuff[i++] = AdcaResult;
        }
        else
        {
            i = 0;
            is_FM_InputDataBuff = 1;
        }
    }

    FM_InputData = AdcaResult;
    FM_DataReady = 1;

    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //clear INT1 flag
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

//    EPwm1Regs.TBCTR = 0; // Clear counter
}

//
// ADCB1_ISR - FRM
//
interrupt void
ADCB1_ISR(void)
{
    meanB = AdcbResultRegs.ADCRESULT1;

    AdcbResult = AdcbResultRegs.ADCRESULT0 - 2048;
    AdcbResults[resultsIndexb++] = AdcbResult;

    if (resultsIndexb == RESULTS_BUFFER_SIZE)
    {
//        tmp = calc_FRM(AdcbResults, RESULTS_BUFFER_SIZE, fc*K);
        resultsIndexb = 0;
        bufferbFull = 1;
    }

    Uint16 out = ARSF_Update(AdcbResult);

    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //clear INT1 flag
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

//    EPwm2Regs.TBCTR = 0; // Clear counter
}

//
// TIMER0_ISR - Indication
//
interrupt void
TIMER0_ISR(void) // CPU-Timer 0
{
    ++count_BARS;

    bool ind_RD = 0;
    bool ind_00 = 0;
    bool ind_40 = 0;
    bool ind_60 = 0;
    bool ind_70 = 0;
    bool ind_80 = 0;
    bool is_sig = 0;

    Uint8 code = ARSF_Struct.Kod;

    if (ARSF_Struct.Kod)
    {
        switch (ARSF_Struct.Kod & 0x3F) // отбрасываем два старших бита
        {
            case 1:case 2:case 3:case 4:
                ind_80 = 1;
                break;
            case 5:case 6:case 7:case 8:case 9:
                ind_70 = 1;
                break;
            case 10:case 11:case 12:case 13:case 14:case 15:case 16:case 17:case 18:case 19:case 20:case 21:case 22:
                ind_60 = 1;
                break;
            case 23:case 24:case 25:case 26:case 27:case 28:case 29:case 30:case 31:case 32:case 33:case 34:case 35:case 36:case 37:case 38:case 39:case 40:case 41:case 42:case 43:
                ind_40 = 1;
                break;
            case 44:case 45:case 46:case 47:case 48:case 49:case 50:case 51:case 52:case 53:case 54:case 55:case 56:case 57:case 58:case 59:case 60:case 61:case 62:
                ind_00 = 1;
                break;
            case 63:
                ind_00 = 1;
                ind_RD = 1;
                break;
            default:
                break;
        }
        is_sig = 1;
    }
    else if (Visual_Kod_FM != FM_Kod_NF)
    {
        ind_RD = (AmplitudaRD >= FM_Level_RD) ? 1 : 0;
        ind_00 = (Amplituda00 >= FM_Level_00) ? 1 : 0;
        ind_40 = (Amplituda40 >= FM_Level_40) ? 1 : 0;
        ind_60 = (Amplituda60 >= FM_Level_60) ? 1 : 0;
        ind_70 = (Amplituda70 >= FM_Level_70) ? 1 : 0;
        ind_80 = (Amplituda80 >= FM_Level_80) ? 1 : 0;

        bool high_ampl = 0;

        if (AmplitudaRD >= FM_Level_RD * 5) high_ampl = 1;
        if (Amplituda00 >= FM_Level_00 * 5) high_ampl = 1;
        if (Amplituda40 >= FM_Level_40 * 5) high_ampl = 1;
        if (Amplituda60 >= FM_Level_60 * 5) high_ampl = 1;
        if (Amplituda70 >= FM_Level_70 * 5) high_ampl = 1;
        if (Amplituda80 >= FM_Level_80 * 5) high_ampl = 1;

        if (high_ampl)
        {
            ind_RD = (AmplitudaRD >= FM_Level_RD*5) ? 1 : 0;
            ind_00 = (Amplituda00 >= FM_Level_00*5) ? 1 : 0;
            ind_40 = (Amplituda40 >= FM_Level_40*5) ? 1 : 0;
            ind_60 = (Amplituda60 >= FM_Level_60*5) ? 1 : 0;
            ind_70 = (Amplituda70 >= FM_Level_70*5) ? 1 : 0;
            ind_80 = (Amplituda80 >= FM_Level_80*5) ? 1 : 0;
        }
        if (ind_RD || ind_00 || ind_40 || ind_60 || ind_70 || ind_80)
        {
            static long i = 0;
            if (++i > 100)
            {
                is_sig = 1;
                i = 0;
            }
            else
                is_sig = 0;
        }
        else
            is_sig = 0;
    }
    if (isBARS)
    {
        static int count = 0;
        if (++count == 1)
        {
            GPIO_WritePin(LED1_GPIO, !ind_80);
            GPIO_WritePin(LED2_GPIO, !ind_70);
            GPIO_WritePin(LED3_GPIO, !ind_60);
            GPIO_WritePin(LED4_GPIO, !ind_40);
            GPIO_WritePin(LED5_GPIO, !ind_00);
        }
        else if (count < 16)
        {
            GPIO_WritePin(LED1_GPIO, ind_80);
            GPIO_WritePin(LED2_GPIO, ind_70);
            GPIO_WritePin(LED3_GPIO, ind_60);
            GPIO_WritePin(LED4_GPIO, ind_40);
            GPIO_WritePin(LED5_GPIO, ind_00);
        }
        else
        {
            count = 0;
            GPIO_WritePin(LED1_GPIO, ind_80);
            GPIO_WritePin(LED2_GPIO, ind_70);
            GPIO_WritePin(LED3_GPIO, ind_60);
            GPIO_WritePin(LED4_GPIO, ind_40);
            GPIO_WritePin(LED5_GPIO, ind_00);
        }
        GPIO_WritePin(LED6_GPIO, ind_RD);
    }
    else
    {
        if (is_sig)
        {
            GPIO_WritePin(LED1_GPIO, ind_80);
            GPIO_WritePin(LED2_GPIO, ind_70);
            GPIO_WritePin(LED3_GPIO, ind_60);
            GPIO_WritePin(LED4_GPIO, ind_40);
            GPIO_WritePin(LED5_GPIO, ind_00);
            GPIO_WritePin(LED6_GPIO, ind_RD);
            count_BARS_p = count_BARS;
        }
        else
        {
            if (count_BARS > count_BARS_p + 4)
            {
                GPIO_WritePin(LED1_GPIO, ind_80);
                GPIO_WritePin(LED2_GPIO, ind_70);
                GPIO_WritePin(LED3_GPIO, ind_60);
                GPIO_WritePin(LED4_GPIO, ind_40);
                GPIO_WritePin(LED5_GPIO, ind_00);
                GPIO_WritePin(LED6_GPIO, ind_RD);
                count_BARS = 0;
            }
        }
    }

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

//
// End of file
//
